#include "Communicator.h"
#include <exception>
#include <iostream>
#include <numeric>
#include <string>
#include <algorithm>

// using static const instead of macros 
static const unsigned short PORT = 3611;
static const unsigned int IFACE = 0;


Communicator::Communicator(IDatabase* database) : m_handlerFactory(RequestHandlerFactory(LoginManager(database), database, RoomManager(), StatisticsManager(database)))
{
	//this->m_database = database;
	_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__ " - socket");
	}
}

Communicator::~Communicator()
{
	try
	{
		::closesocket(_socket);
	}
	catch (...) {}
}

void Communicator::startHandleRequests()
{
	bindAndListen();

	std::thread thread1(&Communicator::acceptClients, this);
	thread1.detach();

	std::string command = "";

	while (command != EXIT_COMMAND)
	{
		std::cout << "> ";
		std::cin >> command;
		std::for_each(command.begin(), command.end(), [](char& c) {
			c = ::tolower(c);
			});
	}
	exit(0);
}

void Communicator::acceptClients()
{
	while (true)
	{
		TRACE("accepting client...");
		acceptClient();
	}
}

void Communicator::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = IFACE;

	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - bind");
	}

	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - listen");
	}
}

void Communicator::acceptClient()
{
	SOCKET clientSocket = accept(_socket, NULL, NULL);
	if (clientSocket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__);
	}
	TRACE("Client accepted !");

	std::thread clientHandleThread(&Communicator::handleNewClient, this, clientSocket);
	clientHandleThread.detach();
}

void Communicator::handleNewClient(SOCKET clientSocket)
{
	try
	{
		int msgCode = Helper::getMessageTypeCode(clientSocket);
		if (msgCode != EXIT_T)
			firstMessage(build_receive_message(clientSocket, msgCode));
		else
			closesocket(clientSocket);
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was catch in function handleNewClient. socket=" << clientSocket << ", what=" << e.what() << std::endl;
		closesocket(clientSocket);
	}
}

RecvMessage* Communicator::build_receive_message(const SOCKET clientSocket, int msgCode)
{
	RecvMessage* msg = nullptr;
	vector<unsigned char> value;
	value = Helper::getDataPartFromSocket(clientSocket);
	return new RecvMessage(clientSocket, msgCode, value);
}

void Communicator::handleReceivedMessages(SOCKET clientSocket)
{
	RequestResult result;
	RequestInfo info;
	int msgCode = Helper::getMessageTypeCode(clientSocket);

	while (msgCode != EXIT_T && msgCode != LOGOUT_T)
	{
		RecvMessage* message = build_receive_message(clientSocket, msgCode);
		info.id = message->getMessageCode();
		info.buffer = message->getValue();

		if (m_clients[clientSocket]->isRequestRelevant(info))
		{
			result = m_clients[clientSocket]->handleRequest(info);
		}

		m_clients[message->getSock()] = result.newHandler;
		Helper::sendData(message->getSock(), result.buffer);
		msgCode = Helper::getMessageTypeCode(clientSocket);
		delete message;
	}
	
	if (msgCode == LOGOUT_T || msgCode == EXIT_T)
	{
		RecvMessage* message = build_receive_message(clientSocket, msgCode);
		info.id = message->getMessageCode();
		info.buffer = message->getValue();
		result = m_clients[clientSocket]->handleRequest(info);
		m_clients[clientSocket] = result.newHandler;
		Helper::sendData(clientSocket, result.buffer);
	}
	if (msgCode == LOGOUT_T)
	{
		result = m_clients[clientSocket]->handleRequest(info);
		m_clients[clientSocket] = result.newHandler;
		Helper::sendData(clientSocket, result.buffer);
	}
	closesocket(clientSocket);
}

void Communicator::firstMessage(RecvMessage* message)
{
	IRequestHandler* requestHandler = this->m_handlerFactory.createLoginRequestHandler();
	RequestInfo info;
	SOCKET clientSocket = message->getSock();
	info.receivalTime = message->getTime();
	info.id = message->getMessageCode();
	info.buffer = message->getValue();
	RequestResult result = requestHandler->handleRequest(info);
	Helper::sendData(clientSocket, result.buffer);
	delete message;
	if (dynamic_cast<MenuRequestHandler*>(result.newHandler) != nullptr)
	{
		m_clients[clientSocket] = result.newHandler;
		std::thread receivedMessagesThread(&Communicator::handleReceivedMessages, this, clientSocket);
		receivedMessagesThread.detach();
	}
	else
		handleNewClient(clientSocket);
}
