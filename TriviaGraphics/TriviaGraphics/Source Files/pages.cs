﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriviaGraphics.Windows;

namespace TriviaGraphics
{
    public class pages
    {
        private int roomID;
        private String name;
        private String roomName;
        private LoginWindow _loginWindow;
        private SignUpWindow _signupWindow;
        private MainWindow _mainWindow;
        private CreateRoomWindow _createRoom;
        private HighScoresWindow _highScoresWindow;
        private JoinRoomWindow _joinRoomWindow;
        private MyPerformancesWindow _myPerformancesWindow;
        private RoomWindow _roomWindow;
        private RoomMemberWindow _roomMemberWindow;
        private QuestionWindow _questionWindow;
        private ResultWindow _resultWindow;

        public LoginWindow getLoginWindow()
        {
             _loginWindow = new LoginWindow();
            return _loginWindow;
        }


        public void setSignupWindow(SignUpWindow signupWindow)
        {
            this._signupWindow = signupWindow;
        }
        public SignUpWindow getSignupWindow()
        {
            if (_signupWindow == null)
            {
                _signupWindow = new SignUpWindow(this);
            }
            return _signupWindow;
        }

        public void setMainWindow(MainWindow mainWindow)
        {
            this._mainWindow = mainWindow;
        }
        public MainWindow getMainWindow()
        {
            if (_mainWindow == null)
            {
                _mainWindow = new MainWindow(this, name);
            }
            return _mainWindow;
        }

        public void setCreateRoomWindow(CreateRoomWindow createRoomWindow)
        {
            this._createRoom = createRoomWindow;
        }
        public CreateRoomWindow getCreateRoomWindow()
        {
            if (_createRoom == null)
            {
                _createRoom = new CreateRoomWindow(this);
            }
            return _createRoom;
        }

        public void setHighScoresWindow(HighScoresWindow highScoresWindow)
        {
            this._highScoresWindow = highScoresWindow;
        }
        public HighScoresWindow getHighScoresWindow()
        {
            if (_highScoresWindow == null)
            {
                _highScoresWindow = new HighScoresWindow(this);
            }
            return _highScoresWindow;
        }

        public void setJoinRoomWindow(JoinRoomWindow joinRoomWindow)
        {
            this._joinRoomWindow = joinRoomWindow;
        }
        public JoinRoomWindow getJoinRoomWindow()
        {
            if (_joinRoomWindow == null)
            {
                _joinRoomWindow = new JoinRoomWindow(this);
            }
            return _joinRoomWindow;
        }
        
        public void setMyPerformancesWindow(MyPerformancesWindow myPerformances)
        {
            this._myPerformancesWindow = myPerformances;
        }
        public MyPerformancesWindow getMyPerformancesWindow()
        {
            if (_myPerformancesWindow == null)
            {
                _myPerformancesWindow = new MyPerformancesWindow(this);
            }
            return _myPerformancesWindow;
        }

        public void setRoomWindow(RoomWindow roomWindow)
        {
            this._roomWindow = roomWindow;
        }
        public RoomWindow getRoomWindow()
        {
            if (_roomWindow == null)
            {
                _roomWindow = new RoomWindow(this, roomName, roomID);
            }
            return _roomWindow;
        }

        public void setRoomMemberWindow(RoomMemberWindow roomMemberWindow)
        {
            this._roomMemberWindow = roomMemberWindow;
        }
        public RoomMemberWindow getRoomMemberWindow()
        {
            if (_roomMemberWindow == null)
            {
                _roomMemberWindow = new RoomMemberWindow(this, roomName, roomID);
            }
            return _roomMemberWindow;
        }

        public void setQuestionWindow(QuestionWindow questionWindow)
        {
            this._questionWindow = questionWindow;
        }
        public QuestionWindow getQuestionWindow()
        {
            if (_questionWindow == null)
            {
                _questionWindow = new QuestionWindow(this);
            }
            return _questionWindow;
        }

        public void setResultWindow(ResultWindow resultWindow)
        {
            this._resultWindow = resultWindow;
        }
        public ResultWindow getResultWindow()
        {
            if (_resultWindow == null)
            {
                _resultWindow = new ResultWindow(this);
            }
            return _resultWindow;
        }

        public void setName(string name)
        {
            this.name = name;
        }
        public string getName()
        {
            return this.name;
        }
        public void setRoom(string roomName, int ID)
        {
            this.roomName = roomName;
            this.roomID = ID;
        }
    }
}
