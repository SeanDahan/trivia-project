#pragma once
#include "RequestStructs.h"
#include "RequestHandlerFactory.h"

class RequestHandlerFactory;
class IRequestHandler;

class RoomMemberRequestHandler: public IRequestHandler
{
public:
	RoomMemberRequestHandler(RequestHandlerFactory factory);
	virtual ~RoomMemberRequestHandler();

	virtual bool isRequestRelevant(RequestInfo);
	virtual RequestResult handleRequest(RequestInfo);

private:
	RequestResult leaveRoom(RequestInfo);
	RequestResult getRoomState(RequestInfo);

	Room m_room;
	LoggedUser m_user;
	RoomManager& m_roomManager;
	RequestHandlerFactory& m_handlerFactory;
};

