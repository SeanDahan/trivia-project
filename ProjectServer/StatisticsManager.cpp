#include "StatisticsManager.h"

StatisticsManager::StatisticsManager()
{
	this->m_database = new SqliteDataBase("MyDB.sqlite");
}

StatisticsManager::StatisticsManager(IDatabase* database) : m_database(database)
{
}

StatisticsManager::~StatisticsManager()
{
}

std::vector<std::string> StatisticsManager::getHighScore() 
{
	return getUserStatistics(this->m_database->getBestPlayerUsername());
}

std::vector<std::string> StatisticsManager::getUserStatistics(std::string username)
{
	std::string averageTime = std::to_string(this->m_database->getPlayerAverageAnswerTime(username));
	std::string correctAmount = std::to_string(this->m_database->getNumOfCorrectAnswer(username));
	std::string totalAmount = std::to_string(this->m_database->getNumOfTotalAnswers(username));
	std::string totalGames = std::to_string(this->m_database->getNumOfPlayerGames(username));
	return std::vector<std::string>({ correctAmount, totalAmount, totalGames, averageTime });
}

void StatisticsManager::setRoomScores(std::vector<LoggedUser> users,int gameId, int amountOfAnswers, std::vector<int> amountOfCorrectAnswers, std::vector<float> averageAnswerTime) 
{
	for (size_t i = 0; i < users.size(); i++) 
		this->m_database->insertScore(users[i].getUsername(), gameId, amountOfAnswers, amountOfCorrectAnswers[i], averageAnswerTime[i]);
}
