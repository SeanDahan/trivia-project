#include "RequestHandlerFactory.h"

RequestHandlerFactory::RequestHandlerFactory(LoginManager loginManager, IDatabase* database, RoomManager roomManager, StatisticsManager statisticsManager) : m_database(database), m_loginManager(loginManager), m_roomManager(roomManager), m_statisticsManager(statisticsManager)
{
}

RequestHandlerFactory::~RequestHandlerFactory()
{
}

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	return new LoginRequestHandler(*this);
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler()
{
	return new MenuRequestHandler(*this);
}


RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler()
{
	return new RoomAdminRequestHandler(*this);
}

RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler()
{
	return new RoomMemberRequestHandler(*this);
}

GameRequestHandler* RequestHandlerFactory::createGameRequestHandler()
{
	return new GameRequestHandler(*this);
}
