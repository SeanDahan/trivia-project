#pragma once
#include <iostream>
#include <vector>
#include <string>
#include "RequestStructs.h"
#include "Helper.h"

#define END_OF_STRING '"'
#define START_STRING 3


class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest(std::vector<unsigned char> Buffer);
	static SignupRequest deserializeSignupRequest(std::vector<unsigned char> Buffer);
	static std::string getData(std::vector<unsigned char> Buffer);
	static std::string getValue(std::string data, std::string key);
	static GetPlayersInRoomRequest deserializeGetPlayersInRoomRequest(std::vector<unsigned char> Buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(std::vector<unsigned char> Buffer);
	static CreateRoomRequest deserializeCreateRoomRequest(std::vector<unsigned char> Buffer);
	static SubmitAnswerRequest deserializeSubmitAnswerRequest(std::vector<unsigned char> Buffer);
};
