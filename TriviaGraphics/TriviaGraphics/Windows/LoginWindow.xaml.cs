﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using System.Net.Sockets;


namespace TriviaGraphics
{
    public partial class LoginWindow : Window
    {
        private String name;
        private pages windows;

        Thread connectionThread;
        public LoginWindow()
        {
            windows = new pages();
            connectionThread = new Thread(initForm);
            connectionThread.Start();
            connectionThread.IsBackground = true;
            InitializeComponent();
        }

        private void initForm()
        {
            global.client = AsynchronousClient.StartClient();
            while (!SocketConnected(global.client))
                global.client = AsynchronousClient.StartClient();
        }

        private bool SocketConnected(Socket s)
        {
            bool part1 = s.Poll(1000, SelectMode.SelectRead);
            bool part2 = (s.Available == 0);
            if (part1 && part2)
                return false;
            else
                return true;
        }

        private void Signup_Click(object sender, RoutedEventArgs e)
        {
            windows.getSignupWindow().Show();
            this.Hide();
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            this.Error.Content = "";

            LoginRequest login = new LoginRequest();
            login.username = this.name = UserNameTB.Text;
            login.password = PasswordTB.Text;
            AsynchronousClient.SendMessage(global.client, JsonRequestPacketSerializer.serializeRequest(login));
            string message = AsynchronousClient.GetMessage(global.client);
            if (JsonResponsePacketDeserializer.findValue(message, "status"))
            {
                this.windows.setName(this.name);
                this.Hide();
                windows.getMainWindow().ShowDialog();
            }
            else 
               this.Error.Content = "Error - Username or Password is incorrect";

        }

        private void UserNameTB_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.Text = string.Empty;
            tb.GotFocus -= UserNameTB_GotFocus;
        }

        private void PasswordTB_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.Text = string.Empty;
            tb.GotFocus -= PasswordTB_GotFocus;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            AsynchronousClient.SendMessage(global.client, JsonRequestPacketSerializer.serializeRequest(global.EXIT_T));
            Application.Current.Shutdown();
        }
    }
}
