#pragma once
#include <WinSock2.h>
#include <deque>
#include <queue>
#include <map>
#include <mutex>
#include <condition_variable>
#include <WinSock2.h>
#include "Helper.h"
#include "IRequestHandler.h"
#include "RecvMessage.h"
#include "LoginRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "LoginManager.h"


#define EXIT_COMMAND "exit"
class Communicator
{
public:
	Communicator(IDatabase* database);
	~Communicator();
	void startHandleRequests();


private:
	void bindAndListen();

	void acceptClient();
	void handleNewClient(SOCKET clientSocket);
	void acceptClients();

	void handleReceivedMessages(SOCKET clientSocket);
	void firstMessage(RecvMessage*);
	static RecvMessage* build_receive_message(SOCKET clientSocket, int msg_code);

	SOCKET _socket;
	RequestHandlerFactory m_handlerFactory;
	std::map<SOCKET, IRequestHandler*> m_clients;
};
