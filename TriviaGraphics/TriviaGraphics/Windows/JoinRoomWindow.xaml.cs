﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Drawing;
using System.Threading;

namespace TriviaGraphics
{
    public partial class JoinRoomWindow : Window
    {
        private string name;
        private int roomID;
        private int index;
        private pages windows;
        private RoomData[] rooms;
        private string[] playersList;

        public JoinRoomWindow()
        {
            InitializeComponent();
        }

        public JoinRoomWindow(pages windows)
        {
            InitializeComponent();
            this.windows = windows;
            this.windows.setJoinRoomWindow(this);
            refreshRoomList();
        }

        private void refreshRoomList()
        {
            AsynchronousClient.SendMessage(global.client, JsonRequestPacketSerializer.serializeRequest(global.GET_ROOMS_T));
            this.rooms = JsonResponsePacketDeserializer.deserializeGetRoomResponse(AsynchronousClient.GetMessage(global.client)).rooms;
            if (this.rooms != null)
            {
                foreach (RoomData room in this.rooms)
                {
                    this.RoomList.Items.Add(room.name);
                }
            }
        }

        private void refreshPlayers()
        {
            this.index = 0;
            getPlayersInRoomRequest getPlayersInRoom = new getPlayersInRoomRequest();
            getPlayersInRoom.roomID = roomID;

            AsynchronousClient.SendMessage(global.client, JsonRequestPacketSerializer.serializeRequest(getPlayersInRoom));
            this.playersList = JsonResponsePacketDeserializer.deserializeGetPlayersResponse(AsynchronousClient.GetMessage(global.client)).players;
            nextFunction();
        }

        private void Join_Click(object sender, RoutedEventArgs e)
        {
            cleanWindow();
            joinRoomRequest joinRoom = new joinRoomRequest();
            joinRoom.roomID = roomID;
            windows.setRoom(name, joinRoom.roomID);
            AsynchronousClient.SendMessage(global.client, JsonRequestPacketSerializer.serializeRequest(joinRoom));
            string message = AsynchronousClient.GetMessage(global.client);
            if (!JsonResponsePacketDeserializer.findValue(message, "ERROR"))
            {
                this.Hide();
                windows.getRoomMemberWindow().ShowDialog();
            }
                //windows.setRoom(createRoom.roomName, global.getID(createRoom.roomName));

            
        }

        private void Refresh_Click(object sender, RoutedEventArgs e)
        {
            refreshRoomList();
            refreshPlayers();
        }

        private void RoomList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int i = 0;
            this.index = 0;
            ComboBox comboBox = (ComboBox)sender;
            string selectedEmployee = (string)comboBox.SelectedItem;
            name = selectedEmployee;

            getPlayersInRoomRequest getPlayersInRoom = new getPlayersInRoomRequest();
            roomID = global.getID(selectedEmployee);
            getPlayersInRoom.roomID = roomID;

            AsynchronousClient.SendMessage(global.client, JsonRequestPacketSerializer.serializeRequest(getPlayersInRoom));
            this.playersList = JsonResponsePacketDeserializer.deserializeGetPlayersResponse(AsynchronousClient.GetMessage(global.client)).players;
            players.Foreground = Brushes.White;
            next_Click(this, e);
        }

        public void cleanWindow()
        {
            index = 0;
            this.RoomList.SelectedIndex = -1;
            next.Content = "";
            previous.Content = "";
            this.name1.Content = "";
            this.name2.Content = "";
            this.name3.Content = "";
        }

        public void changeContent()
        {
            if ((index + 1) > playersList.Length)
                next.Content = "";
            else
                next.Content = "next";

            if ((index - 3) <= 0)
            {
                previous.Content = "";
                if ((index - 3) < 0)
                    index = 0;
            }
            else
                previous.Content = "previous";
        }

        private void nextFunction()
        {
            this.name1.Content = "";
            this.name2.Content = "";
            this.name3.Content = "";
            if (playersList != null && playersList.Length >= index + 1 && index + 1 >= 0)
            {
                this.name1.Content = playersList[index + 0];
                if (playersList.Length >= index + 2 && index + 2 >= 0)
                {
                    this.name2.Content = playersList[index + 1];
                    if (playersList.Length >= index + 3 && index + 3 >= 0)
                    {
                        this.name3.Content = playersList[index + 2];
                    }
                }
                this.index += 3;
                changeContent();
            }
        }

        private void next_Click(object sender, RoutedEventArgs e)
        {
            nextFunction();
        }

        private void previous_Click(object sender, RoutedEventArgs e)
        {
            index -= 6;
            next_Click(sender, e);
            changeContent();
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            cleanWindow();
            this.Hide();
            windows.getMainWindow().ShowDialog();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}