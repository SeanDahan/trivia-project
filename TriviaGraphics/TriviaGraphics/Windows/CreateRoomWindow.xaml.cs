﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TriviaGraphics
{
    /// <summary>
    /// Interaction logic for CreateRoom.xaml
    /// </summary>
    public partial class CreateRoomWindow : Window
    {
        private pages windows;

        public CreateRoomWindow()
        {
            InitializeComponent();
        }

        public CreateRoomWindow(pages windows)
        {
            this.windows = windows;
            this.windows.setCreateRoomWindow(this);
            InitializeComponent();
        }

        private void NumPlayersTB_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.Text = string.Empty;
            tb.GotFocus -= NumPlayersTB_GotFocus;
        }
        private void RoomNameTB_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.Text = string.Empty;
            tb.GotFocus -= RoomNameTB_GotFocus;
        }
        private void NumQuestionsTB_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.Text = string.Empty;
            tb.GotFocus -= NumQuestionsTB_GotFocus;
        }
        private void TimeTB_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.Text = string.Empty;
            tb.GotFocus -= TimeTB_GotFocus;
        }

        private void Create_Click(object sender, RoutedEventArgs e)
        {
            createRoomRequest createRoom = new createRoomRequest();
            createRoom.roomName = RoomNameTB.Text;
            createRoom.questionCount = Int16.Parse(NumQuestionsTB.Text);
            createRoom.maxUsers = Int16.Parse(NumPlayersTB.Text);
            createRoom.answerTimeOut = Int16.Parse(TimeTB.Text);

            AsynchronousClient.SendMessage(global.client, JsonRequestPacketSerializer.serializeRequest(createRoom));
            string message = AsynchronousClient.GetMessage(global.client);

            if (JsonResponsePacketDeserializer.findValue(message, "ERROR"))
            {
                this.Error.Content = "Error- one or more of the details you entered are incorrect";
            }
            else
            {
                windows.setRoom(createRoom.roomName, global.getID(createRoom.roomName));
                this.Hide();
                windows.getRoomWindow().ShowDialog();
            }
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            windows.getMainWindow().ShowDialog();
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
