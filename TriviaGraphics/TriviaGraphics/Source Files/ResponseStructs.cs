﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public struct getRoomResponse
{
	public int status;
	public RoomData[] rooms;
};

public struct getPlayersInRoomResponse
{
	public string[] players;
};

public struct highScoreResponse
{
	public int status;
	public string[] statistics;
};

public struct getPersonalStatsResponse
{
	public int status;
	public string[] statistics;
};

public struct GetRoomStateResponse
{
	public int status;
	public bool hasGameBegun;
	public string[] players;
	public int questionCount;
	public int answerTimeOut;
};

public struct LeaveGameResponse
{
	public int status;
};

public struct GetQuestionResponse
{
	public int status;
	public string question;
	public Dictionary<int, string> answers;
};

public struct SubmitAnswerResponse
{
	public int status;
	public int correctAnswerId;
};

public struct PlayerResults
{
	public string username;
	public int correctAnswersCount;
	public int wrongAnswersCount;
	public int averageAnswerTime;
};

public struct GetGameResultsResponse
{
	public int status;
	public PlayerResults[] results;
};

public struct RoomData
{
	public int id;
	public string name;
	public int maxPlayers;
	public int numOfQuestionsInGame;
	public int timePerQuestion;
	public int isActive;
};

