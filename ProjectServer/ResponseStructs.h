#pragma once
#include <string>
#include <vector>
#include <ctime>
#include <map>
#include "Helper.h"
#include "IRequestHandler.h"

class Helper;
class IRequestHandler;
typedef struct LoginResponse 
{
	unsigned int status;
}LoginResponse;


typedef struct SignupResponse 
{
	unsigned int status;
}SignupResponse;
typedef struct ErrorResponse 
{
	std::string message;
}ErrorResponse;

typedef struct PostResponse 
{
	std::string data;
}PostResponse;

typedef struct LogoutResponse 
{
	unsigned int status;
}LogoutResponse;

typedef struct GetPlayersInRoomResponse 
{
	unsigned int status;
	std::vector<std::string> players;
}GetPlayersInRoomResponse;

typedef struct JoinRoomResponse 
{
	unsigned int status;
}JoinRoomResponse;

typedef struct CreateRoomResponse 
{
	unsigned int status;
}CreateRoomResponse;

typedef struct HighScoreResponse 
{
	unsigned int status;
	std::vector<std::string> statistics;
}HighScoreResponse;

typedef struct GetPersonalStatsResponse 
{
	unsigned int status;
	std::vector<std::string> statistics;
}getPersonalStatsResponse;

typedef struct GetRoomsResponse 
{
	unsigned int status;
	std::vector<struct RoomData> rooms;
}GetRoomResponse;

typedef struct CloseRoomResponse 
{
	unsigned int status;
}CloseRoomResponse;

typedef struct StartGameResponse 
{
	unsigned int status;
}StartGameResponse;

typedef struct LeaveRoomResponse 
{
	unsigned int status;
}LeaveRoomResponse;

typedef struct GetRoomStateResponse 
{
	unsigned int status;
	bool hasGameBegun;
	std::vector<std::string> players;
	unsigned int questionCount;
	unsigned int answerTimeOut;
}GetRoomStateResponse;

typedef struct LeaveGameResponse {
	unsigned int status;
}LeaveGameResponse;

typedef struct GetQuestionResponse {
	unsigned int status;
	std::string question;
	std::map<unsigned int, std::string> answers;
}GetQuestionResponse;

typedef struct SubmitAnswerResponse {
	unsigned int status;
	unsigned int correctAnswerId;
}SubmitAnswerResponse;

typedef struct PlayerResults {
	std::string username;
	unsigned int correctAnswersCount;
	unsigned int wrongAnswersCount;
	unsigned int averageAnswerTime;
}PlayerResults ;

typedef struct GetGameResultsResponse {
	unsigned int status;
	std::vector<PlayerResults> results;
}GetGameResultsResponse;






