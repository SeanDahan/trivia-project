﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TriviaGraphics
{
    public partial class SignUpWindow : Window
    {
        private String name;
        private pages windows;

        public SignUpWindow()
        {
            InitializeComponent();
        }

        public SignUpWindow(pages windows)
        {
            this.windows = windows;
            windows.setSignupWindow(this);
            InitializeComponent();
        }

        private void UserNameTB_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.Text = string.Empty;
            tb.GotFocus -= UserNameTB_GotFocus;
        }

        private void PasswordTB_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.Text = string.Empty;
            tb.GotFocus -= PasswordTB_GotFocus;
        }

        private void EmailTB_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.Text = string.Empty;
            tb.GotFocus -= EmailTB_GotFocus;
        }

        private void SignUp_Click(object sender, RoutedEventArgs e)
        {
            SignupRequest signup = new SignupRequest();
            signup.username = this.name = UserNameTB.Text;
            signup.password = PasswordTB.Text;
            signup.email = EmailTB.Text;

            AsynchronousClient.SendMessage(global.client, JsonRequestPacketSerializer.serializeRequest(signup));
            if (JsonResponsePacketDeserializer.findValue(AsynchronousClient.GetMessage(global.client), "status"))
            {
                this.windows.setName(this.name);
                this.UserNameTB.Text = string.Empty;
                this.PasswordTB.Text = string.Empty;
                this.EmailTB.Text = string.Empty;
                windows.getMainWindow().Show();
                this.Hide();
            }
            else
            {
                this.Error.Content = "Error- This username is already taken";
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
