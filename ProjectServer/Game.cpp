#include "Game.h"

Game::Game()
{
}

void Game::addPlayer(LoggedUser user, GameData data)
{
	this->m_players.insert(std::pair<LoggedUser, GameData>(user, data));
}

Question Game::getQuestionForUser(LoggedUser user)
{
	return this->m_players[user].currentQuestion;
}

void Game::submitAnswer(LoggedUser user, std::string answer)
{
	if (this->m_players[user].currentQuestion.getCorrectAnswer() == answer) 
		this->m_players[user].correctAnswerCount++;
	else 
		this->m_players[user].wrongAnswerCount++;
}

void Game::removePlayer(LoggedUser user)
{
	std::map<LoggedUser, GameData>::iterator it = m_players.find(user);
	m_players.erase(it);
}

bool Game::userExist(LoggedUser user)
{
	return m_players.find(user) != m_players.end() ? true : false;
}

bool Game::operator==(const Game& rhs) const
{
	if (this->m_questions.size() != rhs.m_questions.size()) return false;
	for (size_t i = 0; i < this->m_questions.size(); i++)
		if (!(this->m_questions[i] == rhs.m_questions[i])) return false;
	return true;
}

//bool Game::operator==(const Game& rhs) const
//{
//	if (this->m_players.size() != rhs.m_players.size() || this->m_questions.size() != rhs.m_questions.size()) return false;
//	return std::equal(this->m_players.begin(), this->m_players.end(), rhs.m_players.begin()) && 
//		std::equal(this->m_questions.begin(), this->m_questions.end(), rhs.m_questions.begin());
//}

