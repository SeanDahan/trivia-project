#pragma once
#include <WinSock2.h>
#include <iostream>
#include <vector>
#include "Game.h"
#include "Room.h"
#include "SqliteDataBase.h"
#include "IDatabase.h"

class GameManager
{
public:
	GameManager();
	GameManager(IDatabase* database);
	virtual ~GameManager();

	std::vector<Game> getGames() { return this->m_games; };

	Game createGame(Room room);
	void deleteGame(LoggedUser user);

private:
	std::vector<Game> m_games;
	IDatabase* m_database;
};

