#pragma once
#include <string>

class LoggedUser
{
public:
	LoggedUser(std::string);
	LoggedUser();
	virtual ~LoggedUser();
	std::string getUsername() const { return m_username; };
	bool operator==(const LoggedUser& rhs) const;
	void operator=(LoggedUser const& other);
	bool operator<(const LoggedUser& rhs) const
	{
		return m_username < rhs.m_username;
	}
private:
	std::string m_username;
};
