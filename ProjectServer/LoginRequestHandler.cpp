#include "LoginRequestHandler.h"

LoginRequestHandler::LoginRequestHandler(RequestHandlerFactory& factory) : m_loginManager(factory.getLoginManager()), m_handlerFactory(factory)
{
}

LoginRequestHandler::~LoginRequestHandler()
{}

bool LoginRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
    if (requestInfo.id == SIGNIN_T || requestInfo.id == SIGNUP_T)
    {
        return true;
    }
    return false;
}

RequestResult LoginRequestHandler::handleRequest(RequestInfo requestInfo)
{
    RequestResult result;
    if (!isRequestRelevant(requestInfo))
    {
        ErrorResponse errorResponse;
        errorResponse.message = "The request is irrelevant";
        result.buffer = JsonResponsePacketSerializer::serializeResponse(errorResponse);
        result.newHandler = nullptr;
        return result;
    }
    else if (requestInfo.id == SIGNIN_T)
    {
       return login(requestInfo);
    }
    return signup(requestInfo);
}

RequestResult LoginRequestHandler::login(RequestInfo info)
{
    RequestResult result;
    LoginRequest login = JsonRequestPacketDeserializer::deserializeLoginRequest(info.buffer);
    if (this->m_loginManager.login(login.username, login.password))
    {   
        LoginResponse loginResponse;
        loginResponse.status = SIGNIN_T;
        result.buffer = JsonResponsePacketSerializer::serializeResponse(loginResponse);
        result.newHandler = this->m_handlerFactory.createMenuRequestHandler();
    }
    else
    {
        ErrorResponse errorResponse;
        errorResponse.message = "Wrong Password or username for " + login.username;
        result.buffer = JsonResponsePacketSerializer::serializeResponse(errorResponse);
        result.newHandler = this;
    }
    return result;
}

RequestResult LoginRequestHandler::signup(RequestInfo info)
{
    RequestResult result;
    SignupRequest signup = JsonRequestPacketDeserializer::deserializeSignupRequest(info.buffer);
    if (this->m_loginManager.signup(signup.username, signup.password, signup.email))
    {
        SignupResponse signupResponse;
        signupResponse.status = SIGNUP_T;
        result.buffer = JsonResponsePacketSerializer::serializeResponse(signupResponse);
        result.newHandler = this->m_handlerFactory.createMenuRequestHandler();
    }
    else
    {
        ErrorResponse errorResponse;
        errorResponse.message = "Wrong The username " + signup.username + " is already taken";
        result.buffer = JsonResponsePacketSerializer::serializeResponse(errorResponse);
        result.newHandler = nullptr;
    }
    return result;
}
