#pragma once
#include "Communicator.h"
#include "RequestHandlerFactory.h"
#include "IDatabase.h" 
#include "SqliteDataBase.h"

class Server 
{
public:
	Server();
	virtual ~Server();

	void run();
private:
	IDatabase* m_database;
	Communicator* m_communicator;
};
