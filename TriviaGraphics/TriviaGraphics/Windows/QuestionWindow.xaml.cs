﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Windows.Shapes;

namespace TriviaGraphics.Windows
{
    public partial class QuestionWindow : Window
    {
        private pages windows;
        private string question;
        private DispatcherTimer _timer;
        private TimeSpan _time;
        private Dictionary<int, string> possibleAnswers;

        public QuestionWindow()
        {
            InitializeComponent();
        }

        public QuestionWindow(pages windows)
        {
            InitializeComponent();
            this.windows = windows;
            this.windows.setQuestionWindow(this);
            AsynchronousClient.SendMessage(global.client, JsonRequestPacketSerializer.serializeRequest(global.GET_QUESTION_T));
            GetQuestionResponse response = JsonResponsePacketDeserializer.deserializeGetQuestionResponse(AsynchronousClient.GetMessage(global.client));
            if(response.status == 1)
            {
                this.possibleAnswers = response.answers;
                this.question = response.question;
                showQuestion();
                startTimer();
            }
            else
            {
                this.Hide();
                windows.getResultWindow().ShowDialog();
            }
        }

        public void startTimer()
        {
            _time = TimeSpan.FromSeconds(15);

            _timer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
            {
                tbTime.Text = _time.ToString("c");
                if (_time == TimeSpan.Zero) _timer.Stop();
                _time = _time.Add(TimeSpan.FromSeconds(-1));
            }, Application.Current.Dispatcher);

            _timer.Start();
        }

        public void showQuestion()
        {
            this.Question.Content = question;
            this.Answer1.Content = possibleAnswers[1];
            this.Answer3.Content = possibleAnswers[2];
            this.Answer4.Content = possibleAnswers[3];
            this.Answer2.Content = possibleAnswers[4];
        }

        private void Answer4_Click(object sender, RoutedEventArgs e)
        {
            sendAnswer(4);
        }

        private void Answer3_Click(object sender, RoutedEventArgs e)
        {
            sendAnswer(3);
        }

        private void Answer2_Click(object sender, RoutedEventArgs e)
        {
            sendAnswer(2);
        }

        private void Answer1_Click(object sender, RoutedEventArgs e)
        {
            sendAnswer(1);
        }

        public void sendAnswer(int answerId)
        {
            SubmitAnswerRequest request;
            request.answerId = answerId;
            AsynchronousClient.SendMessage(global.client, JsonRequestPacketSerializer.serializeRequest(request));
            JsonResponsePacketDeserializer.deserializeHighScoreResponse(AsynchronousClient.GetMessage(global.client));
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
