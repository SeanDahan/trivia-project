﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TriviaGraphics
{
    public partial class MyPerformancesWindow : Window
    {
        private pages windows;
        private string[] statistics;

        public MyPerformancesWindow()
        {
            InitializeComponent();
        }

        public MyPerformancesWindow(pages windows)
        {
            this.windows = windows;
            this.windows.setMyPerformancesWindow(this);
            AsynchronousClient.SendMessage(global.client, JsonRequestPacketSerializer.serializeRequest(global.GET_PERSONAL_STATS_T));
            this.statistics = JsonResponsePacketDeserializer.deserializePersonalStatsResponse(AsynchronousClient.GetMessage(global.client)).statistics;
            showMyPerformances(Int16.Parse(statistics[0]), Int16.Parse(statistics[1]), Int16.Parse(statistics[2]), Convert.ToDouble(statistics[3]));
            InitializeComponent();
        }

        public void showMyPerformances(int numGames, int numRight, int numWrong, double average)
        {
            InitializeComponent();
            this.NumGames.Content = "Number of games- " + numGames;
            this.NumRightAns.Content = "Number of right answers- " + numRight;
            this.NumWrongAns.Content = "Number of wrong answers- " + numWrong;
            this.AverageTime.Content = "Average time for answer- " + average;
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            windows.getMainWindow().ShowDialog();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
