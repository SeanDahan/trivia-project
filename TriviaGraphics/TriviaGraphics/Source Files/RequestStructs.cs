public struct LoginRequest 
{
	public string username;
	public string password;
};

public struct SignupRequest 
{
	public string username;
	public string password;
	public string email;
};

public struct getPlayersInRoomRequest
{
	public int roomID;
};

public struct joinRoomRequest
{
	public int roomID;
};

public struct createRoomRequest
{
	public string roomName;
	public int maxUsers;
	public int questionCount;
	public int answerTimeOut;
};

public struct SubmitAnswerRequest
{
	public int answerId;
};
