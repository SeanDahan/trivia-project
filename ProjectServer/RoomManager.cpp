#include "RoomManager.h"

void RoomManager::createRoom(LoggedUser user, struct RoomData newRoom)
{
    if (this->m_rooms.empty())
    {
        newRoom.id = 1;
    }
    else
    {
        newRoom.id = this->m_rooms.rbegin()->first + 1;
    }
    this->m_rooms.insert(std::pair<unsigned int, Room>(newRoom.id, Room(newRoom)));
    this->m_rooms[newRoom.id].addUser(user);
}

void RoomManager::deleteRoom(int ID)
{
    std::map<unsigned int, Room>::iterator it = this->m_rooms.find(ID);
    this->m_rooms.erase(it);
}

unsigned int RoomManager::getRoomState(int ID)
{
    for (std::map<unsigned int, Room>::iterator itr = this->m_rooms.begin(); itr != this->m_rooms.end(); itr++)
    {
        if (itr->second.getID() == ID)
        {
            return itr->second.getActive();
        }
    }
    return 0;
}

std::vector<RoomData> RoomManager::getRooms()
{
    std::vector<RoomData> rooms;
    for (std::map<unsigned int, Room>::iterator itr = this->m_rooms.begin(); itr != this->m_rooms.end(); itr++)
    {
        if (itr->second.getActive() == 0 && itr->second.getRoomData().maxPlayers > itr->second.getAllUsers().size())
        {
            rooms.push_back(itr->second.getRoomData());
        }
    }
    return rooms;
}

std::vector<std::string> RoomManager::getPlayersInRoom(unsigned int id)
{
    for (const auto room : this->m_rooms)
    {
        if (room.first == id)
        {
            std::vector<std::string> players;
            for (const auto player : room.second.getUsers())
            {
                players.push_back(player.getUsername());
            }
            return players;
        }
    }
    return std::vector<std::string>();
}

Room RoomManager::getRoom(unsigned int ID)
{
    return m_rooms[ID];
}

bool RoomManager::roomClose(int ID)
{
    if (m_rooms.find(ID) != m_rooms.end())
        return false;
    return true;
}
