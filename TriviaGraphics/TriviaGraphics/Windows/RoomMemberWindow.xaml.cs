﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TriviaGraphics.Windows
{
    /// <summary>
    /// Interaction logic for RoomMemberWindow.xaml
    /// </summary>
    public partial class RoomMemberWindow : Window
    {
        private int roomID;
        private pages windows;
        private int index;
        private string[] playersList;
        private System.Timers.Timer timerUpdate;

        public RoomMemberWindow()
        {
            InitializeComponent();
        }

        public RoomMemberWindow(pages windows, string roomName, int roomID)
        {
            this.timerUpdate = new System.Timers.Timer(3000);
            timerUpdate.Elapsed += refresh;
            timerUpdate.Enabled = true;
            this.roomID = roomID;
            this.windows = windows;
            this.windows.setRoomMemberWindow(this);
            InitializeComponent();
            Title.Content = "Trivia Game- " + roomName;
        }

        public void cleanWindow()
        {
            index = 0;
            next.Content = "";
            previous.Content = "";
            this.name1.Content = "";
            this.name2.Content = "";
            this.name3.Content = "";
        }

        public void changeContent()
        {
            if ((index + 1) > playersList.Length)
            {
                next.Content = "";
            }
            else
            {
                next.Content = "next";
            }
            if ((index - 3) <= 0)
            {
                previous.Content = "";
                if ((index - 3) < 0)
                {
                    index = 0;
                }
            }
            else
            {
                previous.Content = "previous";
            }
        }

        private void next_Click(object sender, RoutedEventArgs e)
        {
            playersNext();
        }

        private void previous_Click(object sender, RoutedEventArgs e)
        {
            index -= 6;
            next_Click(sender, e);
            changeContent();
        }

        private void playersNext()
        {
            this.name1.Content = "";
            this.name2.Content = "";
            this.name3.Content = "";
            if (playersList != null && playersList.Length >= index + 1 && index + 1 >= 0)
            {
                this.name1.Content = playersList[index + 0];
                if (playersList.Length >= index + 2 && index + 2 >= 0)
                {
                    this.name2.Content = playersList[index + 1];
                    if (playersList.Length >= index + 3 && index + 3 >= 0)
                    {
                        this.name3.Content = playersList[index + 2];
                    }
                }
                this.index += 3;
                changeContent();
            }
        }

        private void refresh(Object source, System.Timers.ElapsedEventArgs e)
        {
            string message = AsynchronousClient.GetMessage(global.client);
            if (JsonResponsePacketDeserializer.findValue(message, global.LEAVE_ROOM_T))
            {
                this.Hide();
                this.timerUpdate.Stop();
                windows.getMainWindow().ShowDialog();
            }
            else if (JsonResponsePacketDeserializer.findValue(message, global.START_GAME_T))
            {
                this.Hide();
                this.timerUpdate.Stop();
                windows.getQuestionWindow().ShowDialog();
            }
            else
            {
                playersList = JsonResponsePacketDeserializer.deserializeRoomStatsResponse(message).players;
            }
        }

        private void Leave_Click(object sender, RoutedEventArgs e)
        {
            AsynchronousClient.SendMessage(global.client, JsonRequestPacketSerializer.serializeRequest(global.LEAVE_ROOM_T));
            JsonResponsePacketDeserializer.deserializeHighScoreResponse(AsynchronousClient.GetMessage(global.client));
            this.Hide();
            this.timerUpdate.Stop();
            windows.getMainWindow().ShowDialog();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            windows.getMainWindow().ShowDialog();
        }
    }
}
