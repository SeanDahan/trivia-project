#pragma once
#include <WinSock2.h>
#include <iostream>
#include <vector>
#include "LoggedUser.h"
#include "SqliteDataBase.h"
#include "IDatabase.h"

class LoginManager 
{
public:
	LoginManager();
	LoginManager(IDatabase* database);
	virtual ~LoginManager();

	LoggedUser getUser(std::string username);
	std::vector<LoggedUser> getLoggedUser();
	bool login(std::string, std::string); // true operation complete, false operation failed and should try again
	bool signup(std::string, std::string, std::string); // true operation complete, false operation failed and should try again
	bool logout(std::string); // true operation complete, false operation failed and should try again
private:
	std::vector<LoggedUser> m_loggedUsers;
	IDatabase* m_database;

};

