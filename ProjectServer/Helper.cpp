#include "Helper.h"

// recieves the type code of the message from socket (3 bytes)
// and returns the code. if no message found in the socket returns 0 (which means the client diclientSocketonnected)
int Helper::getMessageTypeCode(SOCKET clientSocket)
{
	char* s = getPartFromSocket(clientSocket, 2);
	std::string msg(s);
	while (msg == "}" || msg == "\"}" || msg == "")
	{
		s = getPartFromSocket(clientSocket, 2);
		msg = string(s);
	}
	int res = std::atoi(s);
	delete s;
	return  res;
}

void Helper::sendPostMessage(SOCKET clientSocket, const string& data)
{
	PostResponse post = {};
	post.data = data;
	std::vector<unsigned char> json = JsonResponsePacketSerializer::serializeResponse(post);
	sendData(clientSocket, json);
}

//recieve data from socket according byteSize, returns the data as int.
int Helper::getIntPartFromSocket(SOCKET clientSocket, int bytesNum)
{
	char* s = getPartFromSocket(clientSocket, bytesNum, 0);
	return atoi(s);
}

std::vector<byte> Helper::decimalToAscii(int number)
{
	std::vector<byte> output = {};
	while (number)
	{
		output.push_back(number % 256);
		number >>= 8;
	}
	std::reverse(output.begin(), output.end());
	return output;
}

std::vector<byte> Helper::getDataPartFromSocket(SOCKET clientSocket)
{
	int len = getIntPartFromSocket(clientSocket, 4);//Data length.
	std::string response(getPartFromSocket(clientSocket, len));
	return stringToByte(response);
}

std::vector<byte> Helper::stringToByte(std::string data)
{
	std::vector<byte> res;
	std::for_each(data.begin(), data.end(), [&](char lett) {
		res.push_back(static_cast<byte>(lett));
		});
	return res;
}

void Helper::sendError(SOCKET clientSocket, const string& data)
{
	ErrorResponse error;
	error.message = data;
	sendData(clientSocket, JsonResponsePacketSerializer::serializeResponse(error));
}

int Helper::asciiToDecimal(std::vector<byte> ascii)
{
	int output = 0;
	int multiplier = 0;
	std::reverse(ascii.begin(), ascii.end());
	for (auto current : ascii) {
		output += current * pow(256, multiplier);
		multiplier++;
	}
	return output;
}

char* Helper::getPartFromSocket(SOCKET clientSocket, int bytesNum)
{
	//std::this_thread::sleep_for(std::chrono::seconds(3));
	return getPartFromSocket(clientSocket, bytesNum, 0);
}

char* Helper::getPartFromSocket(SOCKET clientSocket, int bytesNum, int flags)
{
	if (bytesNum == 0)
	{
		return (char*)"";
	}

	char* data = new char[bytesNum + 1];
	int res = recv(clientSocket, data, bytesNum, flags);

	if (res == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket: ";
		std::cout << s << std::endl;
		s += std::to_string(clientSocket);
		throw std::exception(s.c_str());
	}

	data[bytesNum] = 0;
	return data;
}

void Helper::sendData(SOCKET clientSocket, std::vector<unsigned char> message)
{
	std::string data = "";
	std::for_each(message.begin(), message.end(), [&](unsigned char lett) {
		data += lett;
		});

	if (send(clientSocket, data.c_str(), message.size(), 0) == INVALID_SOCKET)
	{
		std::cout << "Error while sending message to client " << WSAGetLastError() << std::endl;
		throw std::exception("Error while sending message to client" + WSAGetLastError());
	}
}
