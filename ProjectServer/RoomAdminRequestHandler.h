#pragma once
#include "RequestStructs.h"
#include "RequestHandlerFactory.h"

class RequestHandlerFactory;
class IRequestHandler;

class RoomAdminRequestHandler: public IRequestHandler
{
public:
	RoomAdminRequestHandler(RequestHandlerFactory factory);
	virtual ~RoomAdminRequestHandler();

	virtual bool isRequestRelevant(RequestInfo);
	virtual RequestResult handleRequest(RequestInfo);

private:
	RequestResult closeRoom(RequestInfo);
	RequestResult startGame(RequestInfo);
	RequestResult getRoomState(RequestInfo);

	Room m_room;
	LoggedUser m_user;
	RoomManager& m_roomManager;
	RequestHandlerFactory& m_handlerFactory;
	
};

