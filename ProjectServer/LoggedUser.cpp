#include "LoggedUser.h"

LoggedUser::LoggedUser(std::string username)
{
	this->m_username = username;
}

LoggedUser::LoggedUser()
{
}

LoggedUser::~LoggedUser()
{
}

bool LoggedUser::operator==(const LoggedUser& rhs) const
{
	return this->m_username == rhs.m_username;
}

void LoggedUser::operator=(LoggedUser const& other)
{
	this->m_username = other.getUsername();
}

