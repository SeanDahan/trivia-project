﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TriviaGraphics
{
    /// <summary>
    /// Interaction logic for HighScoresWindow.xaml
    /// </summary>
    public partial class HighScoresWindow : Window
    {
        private pages windows;
        private string[] highScores;

        public HighScoresWindow()
        {
            InitializeComponent();
        }

        public HighScoresWindow(pages windows)
        {
            this.windows = windows;
            this.windows.setHighScoresWindow(this);
            AsynchronousClient.SendMessage(global.client, JsonRequestPacketSerializer.serializeRequest(global.HIGH_SCORE_T));
            this.highScores = JsonResponsePacketDeserializer.deserializeHighScoreResponse(AsynchronousClient.GetMessage(global.client)).statistics;
            InitializeComponent();
            showHighScores(highScores[0], highScores[1], highScores[2]);
        }

        public void showHighScores(String first, String second, String third)
        {
            this.firstPlace.Content = first;
            this.secondPlace.Content = second;
            this.thirdPlace.Content = third;
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            windows.getMainWindow().ShowDialog();
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
