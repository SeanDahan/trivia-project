#include "JsonRequestPacketDeserializer.h"
#include <vector>

LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(std::vector<byte> Buffer)
{
	LoginRequest request = {};
	std::string data = getData(Buffer);

	request.password = getValue(data, "password");
	request.username = getValue(data, "username");

	return request;

}

SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(std::vector<unsigned char> Buffer)
{
	SignupRequest request = {};
	std::string data = getData(Buffer);

	request.password = getValue(data, "password");
	request.username = getValue(data, "username");
	request.email = getValue(data, "email");

	return request;

}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(std::vector<unsigned char> Buffer)
{
	GetPlayersInRoomRequest request = {};
	std::string data = getData(Buffer);

	request.roomID = std::stoi(getValue(data, "roomID"));

	return request;
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(std::vector<unsigned char> Buffer)
{
	JoinRoomRequest request = {};
	std::string data = getData(Buffer);

	request.roomID = std::stoi(getValue(data, "roomID"));

	return request;
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(std::vector<unsigned char> Buffer)
{
	CreateRoomRequest request = {};
	std::string data = getData(Buffer);

	request.answerTimeOut = std::stoi(getValue(data, "answerTimeOut"));
	request.maxUsers = std::stoi(getValue(data, "maxUsers"));
	request.questionCount = std::stoi(getValue(data, "questionCount"));
	request.roomName = getValue(data, "roomName");

	return request;
}

SubmitAnswerRequest JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(std::vector<unsigned char> Buffer)
{
	SubmitAnswerRequest request = {};
	std::string data = getData(Buffer);

	request.answerId = std::stoi(getValue(data, "answerId"));
	
	return request;
}
std::string JsonRequestPacketDeserializer::getData(std::vector<unsigned char> Buffer)
{
	std::string data = "";
	for (int i = 0; i < Buffer.size(); i++)
	{
		data += Buffer[i];
	}
	return data;
}

std::string JsonRequestPacketDeserializer::getValue(std::string data, std::string key)
{
	int index = data.find(key) + key.length() + START_STRING;
	data = data.substr(index, data.length());
	return data.substr(0, data.find(END_OF_STRING));
}
