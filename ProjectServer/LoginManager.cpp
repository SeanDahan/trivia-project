#include "LoginManager.h"

LoginManager::LoginManager()
{
	this->m_database = new SqliteDataBase("MyDB.sqlite");
}

LoginManager::LoginManager(IDatabase* database) : m_database(database)
{
}

LoginManager::~LoginManager()
{}

LoggedUser LoginManager::getUser(std::string username)
{
	for (LoggedUser user : this->m_loggedUsers)
	{
		if (user.getUsername() == username)
		{
			return user;
		}
	}
}

std::vector<LoggedUser> LoginManager::getLoggedUser()
{
	return m_loggedUsers;
}

bool LoginManager::login(std::string username, std::string password)
{
	if (this->m_database->doesPassMatch(username, password)) 
	{
		this->m_loggedUsers.push_back(LoggedUser(username));
		return true;
	}
	return false;
}

bool LoginManager::signup(std::string username, std::string password, std::string email) 
{
	if (!this->m_database->doesUserExist(username)) 
	{
		this->m_database->addNewUser(username, password, email);
		this->m_loggedUsers.push_back(LoggedUser(username));
		return true;
	}
	return false;
}

bool LoginManager::logout(std::string username) 
{
	int size = this->m_loggedUsers.size();
	this->m_loggedUsers.erase(
		std::remove_if(
			this->m_loggedUsers.begin(),
			this->m_loggedUsers.end(),
			[username](const LoggedUser& e) {
				return username == e.getUsername();
			}),
		this->m_loggedUsers.end()
				);
	return size != this->m_loggedUsers.size();
}
