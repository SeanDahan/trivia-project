using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TriviaGraphics
{
	public class JsonResponsePacketDeserializer
	{
		public static bool findValue(string data, string value)
		{
			return data.Contains(value);
		}

		public static string getValue(string data, string key)
		{
			int index = data.IndexOf(key) + key.Length + global.START_STRING;
			data = data.Substring(index);
			return data.Substring(0, data.IndexOf(global.END_OF_STRING));
		}

		public static getRoomResponse deserializeGetRoomResponse(string data)
		{
			getRoomResponse response = new getRoomResponse();
			while(data == "")
            {
				data = AsynchronousClient.GetMessage(global.client);
			}
			int i = 0;
			string roomsString = getValue(data, "Rooms");
			int count = roomsString.Count(f => f == ',') + 1;
			response.rooms = new RoomData[count];
			string[] rooms = roomsString.Split(separator: new[] { "," }, count, options: StringSplitOptions.None);
			foreach(string roomName in rooms)
            {
				response.rooms[i].name = roomName;
				i++;
            }
			return response;
		}

		public static getPlayersInRoomResponse deserializeGetPlayersResponse(string data)
		{
			getPlayersInRoomResponse response = new getPlayersInRoomResponse();
			while (data == "")
			{
				data = AsynchronousClient.GetMessage(global.client);
			}
			string playersString = getValue(data, "PlayersInRoom");
			int count = playersString.Count(f => f == ',') + 1;
			response.players = new string[count];
			response.players = playersString.Split(separator: new[] { "," }, count, options: StringSplitOptions.None);
			return response;
		}

		public static highScoreResponse deserializeHighScoreResponse(string data)
		{
			highScoreResponse response = new highScoreResponse();
			while (data == "")
			{
				data = AsynchronousClient.GetMessage(global.client);
			}
			string statisticsString = getValue(data, "HighScores");
			int count = statisticsString.Count(f => f == ',') + 1;
			response.statistics = new string[count];
			response.statistics = statisticsString.Split(separator: new[] { "," }, count, options: StringSplitOptions.None);
			return response;
		}

		public static getPersonalStatsResponse deserializePersonalStatsResponse(string data)
		{
			getPersonalStatsResponse response = new getPersonalStatsResponse();
			while (data == "")
			{
				data = AsynchronousClient.GetMessage(global.client);
			}
			string statisticsString = getValue(data, "UserStatistics");
			int count = statisticsString.Count(f => f == ',') + 1;
			response.statistics = new string[count];
			response.statistics = statisticsString.Split(separator: new[] { "," }, count, options: StringSplitOptions.None);
			return response;
		}

		public static GetRoomStateResponse deserializeRoomStatsResponse(string data)
		{
			GetRoomStateResponse response = new GetRoomStateResponse();
			while (data == "")
			{
				data = AsynchronousClient.GetMessage(global.client);
			}
			string playersString = getValue(data, "players");
			int count = playersString.Count(f => f == ',') + 1;
			response.players = new string[count];
			response.players = playersString.Split(separator: new[] { "," }, count, options: StringSplitOptions.None);
			response.answerTimeOut = Int16.Parse(getValue(data, "answerTimeOut"));
			response.questionCount = Int16.Parse(getValue(data, "questionCount"));
			response.hasGameBegun = Int16.Parse(getValue(data, "hasGameBegun")) != 0;
			return response;
		}

		public static GetGameResultsResponse deserializeGameResultResponse(string data)
		{
			GetGameResultsResponse response = new GetGameResultsResponse();
			while (data == "")
			{
				data = AsynchronousClient.GetMessage(global.client);
			}
			response.status = Int16.Parse(getValue(data, "status"));
			if (response.status == 1)
			{
				int count = data.Count(f => f == '}') - 1;
				string[] resultStringArr = new string[count];
				resultStringArr = data.Split(separator: new[] { "}" }, count, options: StringSplitOptions.None);
				response.results = new PlayerResults[count];
				for (int i = 0; i < response.results.Length; i++)
				{
					response.results[i].username = getValue(resultStringArr[i], "username");
					response.results[i].wrongAnswersCount = Int16.Parse(getValue(resultStringArr[i], "wrongAnswersCount"));
					response.results[i].correctAnswersCount = Int16.Parse(getValue(resultStringArr[i], "correctAnswersCount"));
					response.results[i].averageAnswerTime = Int16.Parse(getValue(resultStringArr[i], "averageAnswerTime"));
				}
			}
			return response;
		}

		public static GetQuestionResponse deserializeGetQuestionResponse(string data)
		{
			GetQuestionResponse response = new GetQuestionResponse();
			while (data == "")
			{
				data = AsynchronousClient.GetMessage(global.client);
			}
			response.status = Int16.Parse(getValue(data, "status"));
			if(response.status == 1)
            {
				response.question = getValue(data, "question");
				response.answers.Add(1, getValue(data, "1"));
				response.answers.Add(2, getValue(data, "2"));
				response.answers.Add(3, getValue(data, "3"));
				response.answers.Add(4, getValue(data, "4"));
			}
			return response;
		}
	}
}
