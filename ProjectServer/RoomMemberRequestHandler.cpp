#include "RoomMemberRequestHandler.h"

RoomMemberRequestHandler::RoomMemberRequestHandler(RequestHandlerFactory factory)
    : m_handlerFactory(factory), m_roomManager(factory.getRoomManager())
{
}

RoomMemberRequestHandler::~RoomMemberRequestHandler()
{
}

bool RoomMemberRequestHandler::isRequestRelevant(RequestInfo request)
{
    if (request.id == GET_ROOM_STATE_T || request.id == LEAVE_ROOM_T)
    {
        return true;
    }
    return false;
}

RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo request)
{
    RequestResult result;
    if (!isRequestRelevant(request))
    {
        ErrorResponse errorResponse;
        errorResponse.message = "The request is irrelevant";
        result.buffer = JsonResponsePacketSerializer::serializeResponse(errorResponse);
        result.newHandler = nullptr;
    }
    else if (request.id == LEAVE_ROOM_T)
    {
        result = leaveRoom(request);
        result.newHandler = this;
    }
    else
    {
        result = getRoomState(request);
        result.newHandler = this;
    }
    return result;
}

RequestResult RoomMemberRequestHandler::leaveRoom(RequestInfo request)
{
    RequestResult result;
    LeaveRoomResponse leaveRoomResponse;
    m_room.removeUser(m_user);
    leaveRoomResponse.status = LEAVE_ROOM_T;
    result.buffer = JsonResponsePacketSerializer::serializeResponse(leaveRoomResponse);
    return result;
}

RequestResult RoomMemberRequestHandler::getRoomState(RequestInfo request)
{
    RequestResult result;
    GetRoomStateResponse getRoomStateResponse;
    getRoomStateResponse.hasGameBegun = false;
    getRoomStateResponse.players = this->m_room.getAllUsers();
    getRoomStateResponse.questionCount = this->m_room.getRoomData().numOfQuestionsInGame;
    getRoomStateResponse.answerTimeOut = this->m_room.getRoomData().timePerQuestion;
    getRoomStateResponse.status = GET_ROOM_STATE_T;
    result.buffer = JsonResponsePacketSerializer::serializeResponse(getRoomStateResponse);
    return result;
}
