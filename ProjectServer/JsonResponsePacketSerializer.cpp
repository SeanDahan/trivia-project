#include "JsonResponsePacketSerializer.h"

JsonResponsePacketSerializer::JsonResponsePacketSerializer()
{}

JsonResponsePacketSerializer::~JsonResponsePacketSerializer()
{}

std::vector<byte> JsonResponsePacketSerializer::serializeResponse(ErrorResponse error)
{
	std::map<std::string, std::string> dict{ {"message","ERROR"} };
	std::string JSONRes = JsonResponsePacketSerializer::Parse(dict);
	std::vector<byte> vectorCode = Helper::decimalToAscii(ERROR_T);
	std::string stringCode = "";
	std::for_each(vectorCode.begin(), vectorCode.end(), [&](byte letter) {
		stringCode += letter;
		});
	std::string response = std::to_string(ERROR_T) + char(JSONRes.size()) + JSONRes;
	std::vector<byte> returnExp;
	std::for_each(response.begin(), response.end(), [&](char lett) {
		returnExp.push_back(static_cast<byte>(lett));
		});
	return returnExp;
}


std::vector<byte> JsonResponsePacketSerializer::serializeResponse(LoginResponse login) {
	std::map<std::string, int> dict{ {"status",1} };
	std::string JSONRes = JsonResponsePacketSerializer::Parse(dict);
	std::vector<byte> vectorCode = Helper::decimalToAscii(SIGNIN_T);
	std::string stringCode = "";
	std::for_each(vectorCode.begin(), vectorCode.end(), [&](byte letter) {
		stringCode += letter;
		});
	std::string response = stringCode + char(JSONRes.size()) + JSONRes;
	std::vector<byte> returnExp;
	std::for_each(response.begin(), response.end(), [&](char lett) {
		returnExp.push_back(static_cast<byte>(lett));
		});
	return returnExp;
}

std::vector<byte> JsonResponsePacketSerializer::serializeResponse(SignupResponse signup) {
	std::map<std::string, int> dict{ {"status",1} };
	std::string JSONRes = JsonResponsePacketSerializer::Parse(dict);
	std::vector<byte> vectorCode = Helper::decimalToAscii(SIGNUP_T);
	std::string stringCode = "";
	std::for_each(vectorCode.begin(), vectorCode.end(), [&](byte letter) {
		stringCode += letter;
		});
	std::string response = stringCode + char(JSONRes.size()) + JSONRes;
	std::vector<byte> returnExp;
	std::for_each(response.begin(), response.end(), [&](char lett) {
		returnExp.push_back(static_cast<byte>(lett));
		});
	return returnExp;
}


std::vector<byte> JsonResponsePacketSerializer::serializeResponse(PostResponse post) {
	std::map<std::string, std::string> dict{ {"data",post.data} };
	std::string JSONRes = JsonResponsePacketSerializer::Parse(dict);
	std::vector<byte> vectorCode = Helper::decimalToAscii(POST_T);
	std::string stringCode = "";
	std::for_each(vectorCode.begin(), vectorCode.end(), [&](byte letter) {
		stringCode += letter;
		});
	std::string response = stringCode + char(JSONRes.size()) + JSONRes;
	std::vector<byte> returnExp;
	std::for_each(response.begin(), response.end(), [&](char lett) {
		returnExp.push_back(static_cast<byte>(lett));
		});
	return returnExp;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LogoutResponse mappedValue)
{
	std::map<std::string, int> dict{ {"status", 1} };
	std::string JSONRes = JsonResponsePacketSerializer::Parse(dict);
	std::vector<byte> vectorCode = Helper::decimalToAscii(LOGOUT_T);
	std::string stringCode = "";
	std::for_each(vectorCode.begin(), vectorCode.end(), [&](byte letter) {
		stringCode += letter;
		});
	std::string response = stringCode + char(JSONRes.size()) + JSONRes;
	std::vector<byte> returnExp;
	std::for_each(response.begin(), response.end(), [&](char lett) {
		returnExp.push_back(static_cast<byte>(lett));
		});
	return returnExp;

}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse mappedValue)
{
	std::string rooms = "";
	for (RoomData room : mappedValue.rooms)
	{
		rooms += room.name + ",";
	}
	rooms = rooms.substr(0, rooms.length() - 1);
	std::map<std::string, std::string> dict{ {"Rooms", rooms} };
	std::string JSONRes = JsonResponsePacketSerializer::Parse(dict);
	std::vector<byte> vectorCode = Helper::decimalToAscii(GET_ROOMS_T);
	std::string stringCode = "";
	std::for_each(vectorCode.begin(), vectorCode.end(), [&](byte letter) {
		stringCode += letter;
		});
	std::string response = stringCode + char(JSONRes.size()) + JSONRes;
	std::vector<byte> returnExp;
	std::for_each(response.begin(), response.end(), [&](char lett) {
		returnExp.push_back(static_cast<byte>(lett));
		});
	return returnExp;

}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse mappedValue)
{
	std::string players = "";
	for (std::string player : mappedValue.players)
	{
		players += player + ", ";
	}
	players = players.substr(0, players.length() - 2);
	std::map<std::string, std::string> dict{ {"PlayersInRoom", players} };
	std::string JSONRes = JsonResponsePacketSerializer::Parse(dict);
	std::vector<byte> vectorCode = Helper::decimalToAscii(GET_PLAYERS_IN_ROOM_T);
	std::string stringCode = "";
	std::for_each(vectorCode.begin(), vectorCode.end(), [&](byte letter) {
		stringCode += letter;
		});
	std::string response = stringCode + char(JSONRes.size()) + JSONRes;
	std::vector<byte> returnExp;
	std::for_each(response.begin(), response.end(), [&](char lett) {
		returnExp.push_back(static_cast<byte>(lett));
		});
	return returnExp;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse mappedValue)
{
	std::map<std::string, int> dict{ {"status", 1} };
	std::string JSONRes = JsonResponsePacketSerializer::Parse(dict);
	std::vector<byte> vectorCode = Helper::decimalToAscii(JOIN_ROOM_T);
	std::string stringCode = "";
	std::for_each(vectorCode.begin(), vectorCode.end(), [&](byte letter) {
		stringCode += letter;
		});
	std::string response = stringCode + char(JSONRes.size()) + JSONRes;
	std::vector<byte> returnExp;
	std::for_each(response.begin(), response.end(), [&](char lett) {
		returnExp.push_back(static_cast<byte>(lett));
		});
	return returnExp;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse mappedValue)
{
	std::map<std::string, int> dict{ {"status", 1} };
	std::string JSONRes = JsonResponsePacketSerializer::Parse(dict);
	std::vector<byte> vectorCode = Helper::decimalToAscii(CREATE_ROOM_T);
	std::string stringCode = "";
	std::for_each(vectorCode.begin(), vectorCode.end(), [&](byte letter) {
		stringCode += letter;
		});
	std::string response = stringCode + char(JSONRes.size()) + JSONRes;
	std::vector<byte> returnExp;
	std::for_each(response.begin(), response.end(), [&](char lett) {
		returnExp.push_back(static_cast<byte>(lett));
		});
	return returnExp;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetPersonalStatsResponse mappedValue)
{
	std::string statistics = "";
	for (std::string statistic : mappedValue.statistics)
	{
		statistics += statistic + ",";
	}
	statistics = statistics.substr(0, statistics.length() - 1);
	std::map<std::string, std::string> dict{ {"UserStatistics", statistics} };
	std::string JSONRes = JsonResponsePacketSerializer::Parse(dict);
	std::vector<byte> vectorCode = Helper::decimalToAscii(GET_PERSONAL_STATS_T);
	std::string stringCode = "";
	std::for_each(vectorCode.begin(), vectorCode.end(), [&](byte letter) {
		stringCode += letter;
		});
	std::string response = stringCode + char(JSONRes.size()) + JSONRes;
	std::vector<byte> returnExp;
	std::for_each(response.begin(), response.end(), [&](char lett) {
		returnExp.push_back(static_cast<byte>(lett));
		});
	return returnExp;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(HighScoreResponse mappedValue)
{
	std::string statistics = "";
	for (std::string statistic : mappedValue.statistics)
	{
		statistics += statistic + ",";
	}
	statistics = statistics.substr(0, statistics.length() - 1);
	std::map<std::string, std::string> dict{ {"HighScores", statistics} };
	std::string JSONRes = JsonResponsePacketSerializer::Parse(dict);
	std::vector<byte> vectorCode = Helper::decimalToAscii(HIGH_SCORE_T);
	std::string stringCode = "";
	std::for_each(vectorCode.begin(), vectorCode.end(), [&](byte letter) {
		stringCode += letter;
		});
	std::string response = stringCode + char(JSONRes.size()) + JSONRes;
	std::vector<byte> returnExp;
	std::for_each(response.begin(), response.end(), [&](char lett) {
		returnExp.push_back(static_cast<byte>(lett));
		});
	return returnExp;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse mappedValue)
{
	std::map<std::string, int> dict{ {"status",1} };
	std::string JSONRes = JsonResponsePacketSerializer::Parse(dict);
	std::vector<byte> vectorCode = Helper::decimalToAscii(CLOSE_ROOM_T);
	std::string stringCode = "";
	std::for_each(vectorCode.begin(), vectorCode.end(), [&](byte letter) {
		stringCode += letter;
		});
	std::string response = stringCode + char(JSONRes.size()) + JSONRes;
	std::vector<byte> returnExp;
	std::for_each(response.begin(), response.end(), [&](char lett) {
		returnExp.push_back(static_cast<byte>(lett));
		});
	return returnExp;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(StartGameResponse mappedValue)
{
	std::map<std::string, int> dict{ {"status",1} };
	std::string JSONRes = JsonResponsePacketSerializer::Parse(dict);
	std::vector<byte> vectorCode = Helper::decimalToAscii(START_GAME_T);
	std::string stringCode = "";
	std::for_each(vectorCode.begin(), vectorCode.end(), [&](byte letter) {
		stringCode += letter;
		});
	std::string response = stringCode + char(JSONRes.size()) + JSONRes;
	std::vector<byte> returnExp;
	std::for_each(response.begin(), response.end(), [&](char lett) {
		returnExp.push_back(static_cast<byte>(lett));
		});
	return returnExp;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse mappedValue)
{
	std::map<std::string, int> dict{ {"status",1} };
	std::string JSONRes = JsonResponsePacketSerializer::Parse(dict);
	std::vector<byte> vectorCode = Helper::decimalToAscii(LEAVE_ROOM_T);
	std::string stringCode = "";
	std::for_each(vectorCode.begin(), vectorCode.end(), [&](byte letter) {
		stringCode += letter;
		});
	std::string response = stringCode + char(JSONRes.size()) + JSONRes;
	std::vector<byte> returnExp;
	std::for_each(response.begin(), response.end(), [&](char lett) {
		returnExp.push_back(static_cast<byte>(lett));
		});
	return returnExp;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse mappedValue)
{
	std::string players = "";
	for (std::string player : mappedValue.players)
	{
		players += player + ", ";
	}
	players = players.substr(0, players.length() - 1);
	std::map<std::string, int> dictInt{ {"status",mappedValue.status}, {"answerTimeOut",mappedValue.answerTimeOut}, {"questionCount",mappedValue.questionCount} };
	std::string JSONRes = JsonResponsePacketSerializer::Parse(dictInt);
	JSONRes = JSONRes.substr(0, JSONRes.length() - 1) + ',';
	std::map<std::string, std::string> dictString{ {"hasGameBegun",std::to_string(mappedValue.hasGameBegun)}, {"players",players} };
	JSONRes += JsonResponsePacketSerializer::Parse(dictString).substr(1);
	std::vector<byte> vectorCode = Helper::decimalToAscii(GET_ROOM_STATE_T);
	std::string stringCode = "";
	std::for_each(vectorCode.begin(), vectorCode.end(), [&](byte letter) {
		stringCode += letter;
		});
	std::string response = stringCode + char(JSONRes.size()) + JSONRes;
	std::vector<byte> returnExp;
	std::for_each(response.begin(), response.end(), [&](char lett) {
		returnExp.push_back(static_cast<byte>(lett));
		});
	return returnExp;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetGameResultsResponse response)
{
	if (response.status) {
		std::string JSONRes = "{", usernameJson = "";
		std::map<std::string, unsigned int> dictInt;

		for (const auto userResult : response.results) {
			dictInt["correctAnswersCount"] = userResult.correctAnswersCount;
			dictInt["wrongAnswersCount"] = userResult.wrongAnswersCount;
			dictInt["averageAnswerTime"] = userResult.averageAnswerTime;
			JSONRes += "\"" + userResult.username + "\":" + JsonResponsePacketSerializer::Parse(dictInt) + ",";
			
		}
		JSONRes[JSONRes.length() - 1] = '}';
		std::vector<byte> vectorCode = Helper::decimalToAscii(GET_GAME_RESULT_T);
		std::string stringCode = "";
		std::for_each(vectorCode.begin(), vectorCode.end(), [&](byte letter) {
			stringCode += letter;
			});
		std::string response = stringCode + char(JSONRes.size()) + JSONRes;
		std::vector<byte> returnExp;
		std::for_each(response.begin(), response.end(), [&](char lett) {
			returnExp.push_back(static_cast<byte>(lett));
			});
		return returnExp;
	} else {
		std::map<std::string, int> dict{ {"status",1} };
		std::string JSONRes = JsonResponsePacketSerializer::Parse(dict);
		std::vector<byte> vectorCode = Helper::decimalToAscii(GET_GAME_RESULT_T);
		std::string stringCode = "";
		std::for_each(vectorCode.begin(), vectorCode.end(), [&](byte letter) {
			stringCode += letter;
			});
		std::string response = stringCode + char(JSONRes.size()) + JSONRes;
		std::vector<byte> returnExp;
		std::for_each(response.begin(), response.end(), [&](char lett) {
			returnExp.push_back(static_cast<byte>(lett));
			});
		return returnExp;
	}
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse)
{
	std::map<std::string, int> dict{ {"status",1} };
	std::string JSONRes = JsonResponsePacketSerializer::Parse(dict);
	std::vector<byte> vectorCode = Helper::decimalToAscii(SUBMIT_ANSWER_T);
	std::string stringCode = "";
	std::for_each(vectorCode.begin(), vectorCode.end(), [&](byte letter) {
		stringCode += letter;
		});
	std::string response = stringCode + char(JSONRes.size()) + JSONRes;
	std::vector<byte> returnExp;
	std::for_each(response.begin(), response.end(), [&](char lett) {
		returnExp.push_back(static_cast<byte>(lett));
		});
	return returnExp;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse response)
{
	std::string JSONRes = "";
	if (response.status) {
		JSONRes = JsonResponsePacketSerializer::Parse(std::map<std::string, int>{ { "status", response.status } });
		JSONRes = JSONRes.substr(0, JSONRes.length() - 1) + ",";
		JSONRes += "\"question\":";
		JSONRes += "\"" + response.question + "\"," + JsonResponsePacketSerializer::Parse(response.answers).substr(1);
		std::vector<byte> vectorCode = Helper::decimalToAscii(GET_QUESTION_T);
		std::string stringCode = "";
		std::for_each(vectorCode.begin(), vectorCode.end(), [&](byte letter) {
			stringCode += letter;
			});
		std::string response = stringCode + char(JSONRes.size()) + JSONRes;
		std::vector<byte> returnExp;
		std::for_each(response.begin(), response.end(), [&](char lett) {
			returnExp.push_back(static_cast<byte>(lett));
			});
		return returnExp;
	} else {
		std::map<std::string, int> dict{ {"status", response.status} };
		std::string JSONRes = JsonResponsePacketSerializer::Parse(dict);
		std::vector<byte> vectorCode = Helper::decimalToAscii(GET_QUESTION_T);
		std::string stringCode = "";
		std::for_each(vectorCode.begin(), vectorCode.end(), [&](byte letter) {
			stringCode += letter;
			});
		std::string response = stringCode + char(JSONRes.size()) + JSONRes;
		std::vector<byte> returnExp;
		std::for_each(response.begin(), response.end(), [&](char lett) {
			returnExp.push_back(static_cast<byte>(lett));
			});
		return returnExp;
	}
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LeaveGameResponse)
{
	std::map<std::string, int> dict{ {"status",1} };
	std::string JSONRes = JsonResponsePacketSerializer::Parse(dict);
	std::vector<byte> vectorCode = Helper::decimalToAscii(LEAVE_GAME_T);
	std::string stringCode = "";
	std::for_each(vectorCode.begin(), vectorCode.end(), [&](byte letter) {
		stringCode += letter;
		});
	std::string response = stringCode + char(JSONRes.size()) + JSONRes;
	std::vector<byte> returnExp;
	std::for_each(response.begin(), response.end(), [&](char lett) {
		returnExp.push_back(static_cast<byte>(lett));
		});
	return returnExp;
}

std::string JsonResponsePacketSerializer::Parse(std::map<std::string, std::string> mappedValue) {
	std::string out = "{";
	for (auto& value : mappedValue)
		out += "\"" + value.first + "\":" + "\"" + value.second + "\",";
	out[out.size() - 1] = '}';
	return out;
}

std::string JsonResponsePacketSerializer::Parse(std::map<std::string, int> mappedValue) {
	std::string out = "{";
	for (auto& value : mappedValue)
		out += "\"" + value.first + "\":" + "\"" + std::to_string(value.second) + "\",";
	out[out.size() - 1] = '}';
	return out;
}

std::string JsonResponsePacketSerializer::Parse(std::map<std::string, unsigned int> mappedValue) {
	std::string out = "{";
	for (auto& value : mappedValue)
		out += "\"" + value.first + "\":" + "\"" + std::to_string(value.second) + "\",";
	out[out.size() - 1] = '}';
	return out;
}

std::string JsonResponsePacketSerializer::Parse(std::map<unsigned int, std::string> mappedValue) {
	std::string out = "{";
	for (auto& value : mappedValue)
		out += "\"" + std::to_string(value.first) + "\":" + "\"" + value.second + "\",";
	out[out.size() - 1] = '}';
	return out;
}
