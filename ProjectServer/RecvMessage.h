#pragma once
#include <ctime>    
#include <string>
#include <vector>
#include <Windows.h>

using std::vector;
using std::time_t;

class RecvMessage
{
public:
	RecvMessage(SOCKET sock, int messageCode);
	RecvMessage(SOCKET sock, int messageCode, vector<unsigned char> values);

	SOCKET getSock();
	int getMessageCode();
	time_t getTime();

	vector<unsigned char>& getValue();

private:
	SOCKET _sock;
	int _messageCode;
	time_t _receivalTime;
	vector<unsigned char> _values;
};

