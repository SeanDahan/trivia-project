#include "GameManager.h"

GameManager::GameManager()
{
}

GameManager::GameManager(IDatabase* database) : m_database(database)
{
}

GameManager::~GameManager()
{
}

Game GameManager::createGame(Room room)
{
    Game newGame;
    GameData data(this->m_database->getQuestions(1).front());
    for (LoggedUser user : room.getUsers())
    {
        newGame.addPlayer(user, data);
    }
    this->m_games.push_back(newGame);
    return newGame;
}

void GameManager::deleteGame(LoggedUser user)
{
    for (Game game: this->m_games)
    {
        if(game.userExist(user))
        {
            std::vector<Game>::iterator it = std::find(m_games.begin(), m_games.end(), game);
            this->m_games.erase(it);
        }
    }
}
