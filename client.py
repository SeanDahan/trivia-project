import socket
SERVER_IP = '127.0.0.1'
SERVER_PORT = 3611
HELLO = "Hello"

def main():
    # print(len("{\"username\":\"Yael\",\"password\":\"Yael123\",}"))
    print('hello world')
    with socket.socket() as sock:
        server_address = (SERVER_IP, SERVER_PORT)
        sock.connect(server_address)

        server_msg = sock.recv(1024)
        server_msg = server_msg.decode()
        print(server_msg)
        
        # code = chr(65) + chr(1) + chr(0) + chr(0) + chr(41)
        myList = [chr(1),chr(0),chr(0),chr(0),chr(40)]
        code = "".join(myList)
        if HELLO in server_msg:
            message = code+"{\"username\":\"Yael\",\"password\":\"Yael123\"}"
            print(message)
            sock.sendall(message.encode())
            sock.sendall(message.encode())


if __name__ == "__main__":
    main()
