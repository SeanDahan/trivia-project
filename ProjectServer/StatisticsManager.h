#pragma once
#include <string>
#include <vector>
#include "SqliteDataBase.h"
#include "IDatabase.h"
#include "LoggedUser.h"

class StatisticsManager
{
public:
	StatisticsManager();
	StatisticsManager(IDatabase* database);
	virtual ~StatisticsManager();
	std::vector<std::string> getHighScore();
	std::vector<std::string> getUserStatistics(std::string username);
	void setRoomScores(std::vector<LoggedUser>, int, int, std::vector<int>, std::vector<float>);

private:
	IDatabase* m_database;
};

