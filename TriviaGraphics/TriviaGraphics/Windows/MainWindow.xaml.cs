﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TriviaGraphics
{
    public partial class MainWindow : Window
    {
        private String name;
        private pages windows;

        public MainWindow()
        {
            InitializeComponent();
        }

        public MainWindow(pages windows, string name)
        {
            this.name = name;
            this.windows = windows;
            this.windows.setMainWindow(this);
            InitializeComponent();
            this.Welcome.Content = "- Welcome " + name + " -";
        }

        private void Join_Room_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            windows.getJoinRoomWindow().ShowDialog();
        }

        private void Create_Room_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            windows.getCreateRoomWindow().ShowDialog();
        }

        private void BestScores_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            windows.getHighScoresWindow().ShowDialog();
        }

        private void MyPerformances_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            windows.getMyPerformancesWindow().ShowDialog();
        }

        private void SignOut_Click(object sender, RoutedEventArgs e)
        {
            AsynchronousClient.SendMessage(global.client, JsonRequestPacketSerializer.serializeRequest(global.LOGOUT_T));
            if (JsonResponsePacketDeserializer.findValue(AsynchronousClient.GetMessage(global.client), "status"))
            {
                this.Hide();
                windows.getLoginWindow().ShowDialog();
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Quit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
