#pragma once
#include "LoginManager.h"
#include "IDatabase.h"
#include "RoomMemberRequestHandler.h"
#include "RoomAdminRequestHandler.h"
#include "LoginRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "MenuRequestHandler.h"
#include "StatisticsManager.h"
#include "GameManager.h"
#include "GameRequestHandler.h"

class RoomAdminRequestHandler;
class RoomMemberRequestHandler;
class LoginRequestHandler;
class MenuRequestHandler;
class StatisticsManager;
class GameRequestHandler;
class GameManager;

class RequestHandlerFactory
{
public:
	RequestHandlerFactory(LoginManager loginManager, IDatabase* database, RoomManager roomManager, StatisticsManager statisticsManager);
	virtual ~RequestHandlerFactory();


	
	LoginManager& getLoginManager() { return this->m_loginManager; };
	GameManager& getGameManager() { return this->m_gameManager; };
	RoomManager& getRoomManager() { return this->m_roomManager; };
	StatisticsManager& getStatisticsManager() { return this->m_statisticsManager; };

	LoginRequestHandler* createLoginRequestHandler();
	MenuRequestHandler* createMenuRequestHandler();
	RoomAdminRequestHandler* createRoomAdminRequestHandler();
	RoomMemberRequestHandler* createRoomMemberRequestHandler();
	GameRequestHandler* createGameRequestHandler();

private:
	LoginManager m_loginManager;
	StatisticsManager m_statisticsManager;
	IDatabase* m_database;
	RoomManager m_roomManager;
	GameManager m_gameManager;
};
