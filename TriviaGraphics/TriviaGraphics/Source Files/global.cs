﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TriviaGraphics
{
    class global
    {
		public static Socket client;
		public const string EXIT_T = "00";
		public const string LOGIN_T = "01";
		public const string SIGNUP_T = "02";
		public const string LOGOUT_T = "03";
		public const string CREATE_ROOM_T = "07";
		public const string JOIN_ROOM_T = "08";
		public const string GET_ROOMS_T = "09";
		public const string GET_PLAYERS_IN_ROOM_T = "10";
		public const string HIGH_SCORE_T = "11";
		public const string GET_PERSONAL_STATS_T = "12";
        public const string CLOSE_ROOM_T = "13";
        public const string START_GAME_T = "14";
        public const string LEAVE_ROOM_T = "15";
        public const string GET_ROOM_STATE_T = "16";
        public const string GET_GAME_RESULT_T = "17";
        public const string SUBMIT_ANSWER_T = "18";
        public const string GET_QUESTION_T = "19";
        public const string LEAVE_GAME_T = "20";
        public const int START_STRING = 3;
		public const char END_OF_STRING = '"';

        public static int getID(string roomName)
        {
            AsynchronousClient.SendMessage(global.client, JsonRequestPacketSerializer.serializeRequest(global.GET_ROOMS_T));
            RoomData[] rooms = JsonResponsePacketDeserializer.deserializeGetRoomResponse(AsynchronousClient.GetMessage(global.client)).rooms;
            int i = 0;
            int roomID = 0;
            if (rooms != null)
            {
                foreach (RoomData room in rooms)
                {
                    i++;
                    if (room.name == roomName)
                    {
                        roomID = i;
                    }
                }
            }
            return roomID;
        }
    }

	
}
