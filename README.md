# Trivia Project
- `By Yael Naim & Sean Dahan`

## Structure
- `code` (8 bits)   
- `dataLength` (32 bits)
- `data` (8*dataLength bits)    

Final Length is => (5 + dataLength) byte

### Codes

- Signin 100
- Signup 200
- Error 400
- Post 600 -> { For initial stages } 
