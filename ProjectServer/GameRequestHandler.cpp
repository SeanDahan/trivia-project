#include "GameRequestHandler.h"

GameRequestHandler::GameRequestHandler(RequestHandlerFactory& factory) : m_requestHandleFactory(factory), m_gameManager(factory.getGameManager())
{
}

GameRequestHandler::~GameRequestHandler()
{
}

bool GameRequestHandler::isRequestRelevant(RequestInfo request)
{
    switch (request.id)
    {
    case GET_GAME_RESULT_T:
    case SUBMIT_ANSWER_T:
    case GET_QUESTION_T:
    case LEAVE_GAME_T: return true;
    default: return false;
    }
}

RequestResult GameRequestHandler::handleRequest(RequestInfo request)
{
    switch (request.id) {
    case GET_GAME_RESULT_T: return getQuestion(request);
    case SUBMIT_ANSWER_T: return submitAnswer(request);
    case GET_QUESTION_T: return getGameResults(request);
    case LEAVE_GAME_T: return leaveGame(request);
    default: return RequestResult();
    }
}

RequestResult GameRequestHandler::getQuestion(RequestInfo)
{
    RequestResult result;
    GetQuestionResponse getQuestionResponse;
    getQuestionResponse.status = GET_QUESTION_T;
    getQuestionResponse.question = this->m_game.getQuestionForUser(m_user).getQuestion();
    result.buffer = JsonResponsePacketSerializer::serializeResponse(getQuestionResponse);
    result.newHandler = this;
    return result;
}

RequestResult GameRequestHandler::submitAnswer(RequestInfo info)
{
    RequestResult result;
    SubmitAnswerResponse submitAnswerResponse;
    SubmitAnswerRequest submitAnswerRequest = JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(info.buffer);
    int correctAnswerId = 0;
    GameData userData(Question("", "", "", "", "", 0));
    for (const auto userPair : this->m_game.getPlayers()) {
        if(userPair.first == this->m_user)  userData = userPair.second;
    }
    this->m_game.submitAnswer(this->m_user, userData.currentQuestion.getPossibleAnswers()[submitAnswerRequest.answerId]);
    for (const auto _ : userData.currentQuestion.getPossibleAnswers()) {
        if (userData.currentQuestion.getPossibleAnswers()[correctAnswerId++] == userData.currentQuestion.getCorrectAnswer()) break;
    }
    submitAnswerResponse.correctAnswerId = correctAnswerId;
    submitAnswerResponse.status = 1;
    result.buffer = JsonResponsePacketSerializer::serializeResponse(submitAnswerResponse);
    result.newHandler = this;
    return result;
}

RequestResult GameRequestHandler::getGameResults(RequestInfo request)
{
    
    RequestResult result;
    GetGameResultsResponse getGameResultsResponse;
    getGameResultsResponse.status = GET_GAME_RESULT_T;
    PlayerResults playerResult = {};
     
    for (auto const playerData : this->m_game.getPlayers()) {
        playerResult.username = playerData.first.getUsername();
        playerResult.correctAnswersCount = playerData.second.correctAnswerCount;
        playerResult.averageAnswerTime = playerData.second.averageAnswerTime;
        playerResult.wrongAnswersCount = playerData.second.wrongAnswerCount;
        getGameResultsResponse.results.push_back(playerResult);
    }
    result.buffer = JsonResponsePacketSerializer::serializeResponse(getGameResultsResponse);
    result.newHandler = this->m_requestHandleFactory.createMenuRequestHandler();
    return result;
}

RequestResult GameRequestHandler::leaveGame(RequestInfo)
{
    RequestResult result;
    LeaveGameResponse leaveGameResponse;
    leaveGameResponse.status = LEAVE_GAME_T;
    this->m_game.removePlayer(m_user);
    result.buffer = JsonResponsePacketSerializer::serializeResponse(leaveGameResponse);
    result.newHandler = this->m_requestHandleFactory.createMenuRequestHandler();
    return result;
}
