﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TriviaGraphics.Windows
{
    public partial class ResultWindow : Window
    {
        private pages windows;

        public ResultWindow()
        {
            InitializeComponent();
        }

        public ResultWindow(pages windows)
        {
            InitializeComponent();
            this.windows = windows;
            this.windows.setResultWindow(this);
            AsynchronousClient.SendMessage(global.client, JsonRequestPacketSerializer.serializeRequest(global.GET_GAME_RESULT_T));
            GetGameResultsResponse response = JsonResponsePacketDeserializer.deserializeGameResultResponse(AsynchronousClient.GetMessage(global.client));
            if (response.status == 1)
            {
                for(int i = 0; i < response.results.Length; i++)
                {
                    if (response.results[i].username == windows.getName())
                    {
                        this.correctAnswersCount.Content = "Correct answers count- " + response.results[i].correctAnswersCount;
                        this.wrongAnswersCount.Content = "Wrong answers count- " + response.results[i].wrongAnswersCount;
                        this.averageAnswerTime.Content = "Average answer time- " + response.results[i].averageAnswerTime;
                    }
                }
            }
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            windows.getMainWindow().ShowDialog();
        }
    }
}