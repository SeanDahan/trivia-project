#pragma once
#include "ResponseStructs.h"
struct LoginResponse;
struct SignupResponse;
struct ErrorResponse;
struct PostResponse;
struct RequestInfo;
struct RequestResult;
struct LoginRequest;
struct SignupRequest;
class IRequestHandler
{
public:
	IRequestHandler();
	~IRequestHandler(); 

	virtual bool isRequestRelevant(RequestInfo) = 0;
	virtual RequestResult handleRequest(RequestInfo) = 0;
private:
};

