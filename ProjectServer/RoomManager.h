#include "room.h"
#include <map>
#include <utility>

class RoomManager
{
public:
    void createRoom(LoggedUser, struct RoomData);
    void deleteRoom(int ID);
    unsigned int getRoomState(int ID);
    std::vector<struct RoomData> getRooms();
    std::vector<std::string> getPlayersInRoom(unsigned int id);
    Room getRoom(unsigned int ID);
    bool roomClose(int ID);

private:
    std::map<unsigned int, Room> m_rooms;
};
