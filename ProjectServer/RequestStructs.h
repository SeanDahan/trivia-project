#pragma once
#include <string>
#include <vector>
#include <ctime>
#include "Helper.h"
#include "IRequestHandler.h"
class Helper;
class IRequestHandler;

typedef struct RequestInfo 
{
	int id;
	time_t receivalTime; // HELP => Print { ctime(&receivalTime) } ; Creation { time(&receivalTime) }
	std::vector<unsigned char> buffer;
}RequestInfo;

typedef struct RequestResult 
{
	std::vector<unsigned char> buffer;
	IRequestHandler* newHandler;
}RequestResult;

typedef struct LoginRequest 
{
	std::string username;
	std::string password;
}LoginRequest;

typedef struct SignupRequest 
{
	std::string username;
	std::string password;
	std::string email;
}SignupRequest;

typedef struct GetPlayersInRoomRequest 
{
	unsigned int roomID;
}GetPlayersInRoomRequest;

typedef struct JoinRoomRequest 
{
	unsigned int roomID;
}JoinRoomRequest;

typedef struct CreateRoomRequest 
{
	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeOut;
}CreateRoomRequest;

typedef struct SubmitAnswerRequest {
	unsigned int answerId;
}SubmitAnswerRequest;
