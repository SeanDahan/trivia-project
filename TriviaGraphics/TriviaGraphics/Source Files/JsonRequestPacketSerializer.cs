using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TriviaGraphics
{
	public class JsonRequestPacketSerializer
    {

		public static byte[] serializeRequest(LoginRequest login)
		{
			Dictionary<string, string> dict = new Dictionary<string, string>();
			dict.Add("username", login.username);
			dict.Add("password", login.password);
			string JSONRes = Parse(dict);
			string msg = global.LOGIN_T + getPaddedNumber(JSONRes.Length.ToString(), 4) + JSONRes;
			byte[] t = Encoding.ASCII.GetBytes(global.LOGIN_T + getPaddedNumber(JSONRes.Length.ToString(), 4) + JSONRes);
			byte[] inBuffer = new byte[t.Length + 1];
			for (int i = 0; i < t.Length; i++)
			{
				inBuffer[i] = t[i];
			}
			inBuffer[inBuffer.Length - 1] = 0;
			return inBuffer;
		}

		public static byte[] serializeRequest(SignupRequest signup)
		{
			Dictionary<string, string> dict = new Dictionary<string, string>();
			dict.Add("username", signup.username);
			dict.Add("password", signup.password);
			dict.Add("email", signup.email);
			string JSONRes = Parse(dict);
			byte[] t = Encoding.ASCII.GetBytes(global.SIGNUP_T + getPaddedNumber(JSONRes.Length.ToString(), 4) + JSONRes);
			byte[] inBuffer = new byte[t.Length + 1];
			for (int i = 0; i < t.Length; i++)
			{
				inBuffer[i] = t[i];
			}
			inBuffer[inBuffer.Length - 1] = 0;
			return inBuffer;
		}

		public static byte[] serializeRequest(getPlayersInRoomRequest getPlayersInRoom)
		{
			Dictionary<string, int> dict = new Dictionary<string, int>();
			dict.Add("roomID", getPlayersInRoom.roomID);
			string JSONRes = Parse(dict);
			byte[] t = Encoding.ASCII.GetBytes(global.GET_PLAYERS_IN_ROOM_T + getPaddedNumber(JSONRes.Length.ToString(), 4) + JSONRes);
			byte[] inBuffer = new byte[t.Length + 1];
			for (int i = 0; i < t.Length; i++)
			{
				inBuffer[i] = t[i];
			}
			inBuffer[inBuffer.Length - 1] = 0;
			return inBuffer;
		}

		public static byte[] serializeRequest(joinRoomRequest joinRoom)
		{
			Dictionary<string, int> dict = new Dictionary<string, int>();
			dict.Add("roomID", joinRoom.roomID);
			string JSONRes = Parse(dict);
			string str = global.JOIN_ROOM_T + getPaddedNumber(JSONRes.Length.ToString(), 4) + JSONRes;
			byte[] t = Encoding.ASCII.GetBytes(global.JOIN_ROOM_T + getPaddedNumber(JSONRes.Length.ToString(), 4) + JSONRes);
			byte[] inBuffer = new byte[t.Length + 1];
			for (int i = 0; i < t.Length; i++)
			{
				inBuffer[i] = t[i];
			}
			inBuffer[inBuffer.Length - 1] = 0;
			return inBuffer;
		}

		public static byte[] serializeRequest(createRoomRequest createRoom)
		{
			Dictionary<string, string> dictString = new Dictionary<string, string>();
			dictString.Add("roomName", createRoom.roomName);
			string JSONResRoomName = Parse(dictString);
			JSONResRoomName = JSONResRoomName.Substring(0, JSONResRoomName.Length - 1);
			Dictionary<string, int> dictInt = new Dictionary<string, int>();
			dictInt.Add("maxUsers", createRoom.maxUsers);
			dictInt.Add("questionCount", createRoom.questionCount);
			dictInt.Add("answerTimeOut", createRoom.answerTimeOut);
			string JSONRes = JSONResRoomName + ',' + Parse(dictInt).Substring(1);
			byte[] t = Encoding.ASCII.GetBytes(global.CREATE_ROOM_T + getPaddedNumber(JSONRes.Length.ToString(), 4) + JSONRes);
			byte[] inBuffer = new byte[t.Length + 1];
			for (int i = 0; i < t.Length; i++)
			{
				inBuffer[i] = t[i];
			}
			inBuffer[inBuffer.Length - 1] = 0;
			return inBuffer;
		}

		public static byte[] serializeRequest(SubmitAnswerRequest submitAnswer)
		{
			Dictionary<string, int> dict = new Dictionary<string, int>();
			dict.Add("answerId", submitAnswer.answerId);
			string JSONRes = Parse(dict);
			byte[] t = Encoding.ASCII.GetBytes(global.SUBMIT_ANSWER_T + getPaddedNumber(JSONRes.Length.ToString(), 4) + JSONRes);
			byte[] inBuffer = new byte[t.Length + 1];
			for (int i = 0; i < t.Length; i++)
			{
				inBuffer[i] = t[i];
			}
			inBuffer[inBuffer.Length - 1] = 0;
			return inBuffer;
		}

		public static byte[] serializeRequest(string code)
		{
			byte[] t = Encoding.ASCII.GetBytes(code + getPaddedNumber("0", 4));
			byte[] inBuffer = new byte[t.Length + 1];
			for (int i = 0; i < t.Length; i++)
			{
				inBuffer[i] = t[i];
			}
			inBuffer[inBuffer.Length - 1] = 0;
			return inBuffer;
		}

		public static string Parse(Dictionary<string, string> mappedValue)
		{
			string strOut = "{";
			foreach (KeyValuePair<string, string> value in mappedValue)
			{
				strOut += "\"" + value.Key + "\":" + "\"" + value.Value + "\",";
			}
			strOut = strOut.Remove(strOut.Length - 1);
			strOut += '}';
			return strOut;
		}

		public static string Parse(Dictionary<string, int> mappedValue)
		{
			string strOut = "{";
			foreach (KeyValuePair<string, int> value in mappedValue)
			{
				strOut += "\"" + value.Key + "\":" + "\"" + value.Value + "\",";
			}
			strOut = strOut.Remove(strOut.Length - 1);
			strOut += '}';
			return strOut;
		}

		// return string after padding zeros if necessary
		public static string getPaddedNumber(string num, int digits)
		{
			string str = "";
			for(int i = 0; num.Length + i < digits; i++)
            {
				str += '0';
            }
			str += num;
			return str;
		}
	}
}

