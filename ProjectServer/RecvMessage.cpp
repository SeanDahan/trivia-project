#include "RecvMessage.h"

RecvMessage::RecvMessage(SOCKET sock, int messageCode)
{
	_sock = sock;
	_messageCode = messageCode;
	_receivalTime = std::time(0);
}

RecvMessage::RecvMessage(SOCKET sock, int messageCode, vector<unsigned char> values) : RecvMessage(sock, messageCode)
{
	_values = values;
}

SOCKET RecvMessage::getSock()
{
	return _sock;
}


int RecvMessage::getMessageCode()
{
	return _messageCode;
}

time_t RecvMessage::getTime()
{
	return this->_receivalTime;
}

vector<unsigned char>& RecvMessage::getValue()
{
	return _values;
}
