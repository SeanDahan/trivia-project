 #pragma once
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include "RoomManager.h"
#include "Helper.h"
#include "ResponseStructs.h"

class Helper;
struct LoginResponse;
struct SignupResponse;
struct ErrorResponse;
struct PostResponse;
struct RequestInfo;
struct RequestResult;
struct LoginRequest;
struct SignupRequest;
struct LogoutResponse;
struct GetRoomsResponse;
struct GetPlayersInRoomResponse;
struct JoinRoomResponse;
struct CreateRoomResponse;
struct HighScoreResponse;
struct GetPersonalStatsResponse;
struct CloseRoomResponse;
struct StartGameResponse;
struct LeaveRoomResponse;
struct GetRoomStateResponse;
struct GetGameResultsResponse;
struct GetQuestionResponse;
struct LeaveGameResponse;
struct SubmitAnswerResponse;


class JsonResponsePacketSerializer
{
public:
	JsonResponsePacketSerializer();
	virtual ~JsonResponsePacketSerializer();

	static std::vector<unsigned char> serializeResponse(ErrorResponse);
	static std::vector<unsigned char> serializeResponse(LoginResponse);
	static std::vector<unsigned char> serializeResponse(SignupResponse);
	static std::vector<unsigned char> serializeResponse(PostResponse);
	static std::vector<unsigned char> serializeResponse(LogoutResponse);
	static std::vector<unsigned char> serializeResponse(GetRoomsResponse);
	static std::vector<unsigned char> serializeResponse(GetPlayersInRoomResponse);
	static std::vector<unsigned char> serializeResponse(JoinRoomResponse);
	static std::vector<unsigned char> serializeResponse(CreateRoomResponse);
	static std::vector<unsigned char> serializeResponse(GetPersonalStatsResponse);
	static std::vector<unsigned char> serializeResponse(HighScoreResponse);
	static std::vector<unsigned char> serializeResponse(CloseRoomResponse);
	static std::vector<unsigned char> serializeResponse(StartGameResponse);
	static std::vector<unsigned char> serializeResponse(LeaveRoomResponse);
	static std::vector<unsigned char> serializeResponse(GetRoomStateResponse);
	static std::vector<unsigned char> serializeResponse(GetGameResultsResponse);
	static std::vector<unsigned char> serializeResponse(SubmitAnswerResponse);
	static std::vector<unsigned char> serializeResponse(GetQuestionResponse);
	static std::vector<unsigned char> serializeResponse(LeaveGameResponse);

	static std::string Parse(std::map<std::string, std::string>);
	static std::string Parse(std::map<std::string, int>);
	static std::string Parse(std::map<std::string, unsigned int>);
	static std::string Parse(std::map<unsigned int, std::string>);

private:

};
