#pragma once
#include "IDatabase.h"
#include <string>
#include <vector>
#include "sqlite3.h"
#include <io.h>
#include <iostream>
typedef int (*callbackFunction)(void*, int, char**, char**);

class SqliteDataBase : public IDatabase
{
public:
    // Class
    SqliteDataBase(const char*);
    SqliteDataBase();
    virtual ~SqliteDataBase();


    // Custom
    bool doesUserExist(std::string);
    bool doesPassMatch(std::string, std::string);
    bool addNewUser(std::string, std::string, std::string);


    virtual std::list<Question> getQuestions(int);
    virtual float getPlayerAverageAnswerTime(std::string);
    virtual int getNumOfCorrectAnswer(std::string);
    virtual int getNumOfTotalAnswers(std::string);
    virtual std::string getBestPlayerUsername();
    virtual int getNumOfPlayerGames(std::string);

    virtual void insertScore(std::string username, int gameId, int amountOfAnswers, int amountOfCorrectAnswers, float averageAnswerTime);

private:

    sqlite3* dbPointer = nullptr;

    // Basic
    bool executeQuery(const char*, callbackFunction, void*);
    static int singleIntCallBack(void*, int, char**, char**);
    static int singleBoolCallBack(void*, int, char**, char**);
    static int singleFloatCallBack(void*, int, char**, char**);
    static int questionsCallback(void*, int, char**, char**);
    static int singleStringCallback(void* data, int argc, char** argv, char** azColName);
};

