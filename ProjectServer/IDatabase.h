#pragma once
#include <string>
#include "Question.h"
#include <list>

typedef int (*callbackFunction)(void*, int, char**, char**);
class IDatabase
{
    // Basic
    virtual bool executeQuery(const char*, callbackFunction, void*) = 0;
public:
    IDatabase();
    virtual ~IDatabase();

    // Custom
    virtual bool doesUserExist(std::string) = 0;
    virtual bool doesPassMatch(std::string, std::string) = 0;
    virtual bool addNewUser(std::string, std::string, std::string) = 0;
    virtual std::list<Question> getQuestions(int) = 0;
    virtual std::string getBestPlayerUsername() = 0;
    virtual float getPlayerAverageAnswerTime(std::string) = 0;
    virtual int getNumOfCorrectAnswer(std::string) = 0;
    virtual int getNumOfTotalAnswers(std::string) = 0;
    virtual int getNumOfPlayerGames(std::string) = 0;
    virtual void insertScore(std::string username, int gameId, int amountOfAnswers, int amountOfCorrectAnswers, float averageAnswerTime) = 0;
};
