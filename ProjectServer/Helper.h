#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <vector>
#include <string>
#include <WinSock2.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <thread>
#include "ResponseStructs.h"
#include "JsonResponsePacketSerializer.h"

using std::string;


enum RequestId
{
	SIGNIN_T = 1,
	SIGNUP_T = 2,
	LOGOUT_T = 3,
	ERROR_T = 4,
	POST_T = 6,
	EXIT_T = 0,
	CREATE_ROOM_T = 7,
	JOIN_ROOM_T = 8,
	GET_ROOMS_T = 9,
	GET_PLAYERS_IN_ROOM_T = 10,
	HIGH_SCORE_T = 11,
	GET_PERSONAL_STATS_T = 12,
	CLOSE_ROOM_T = 13,
	START_GAME_T = 14,
	LEAVE_ROOM_T = 15,
	GET_ROOM_STATE_T = 16,
	GET_GAME_RESULT_T = 17,
	SUBMIT_ANSWER_T = 18,
	GET_QUESTION_T = 19,
	LEAVE_GAME_T = 20,
};


class JsonResponsePacketSerializer;
class RequestHandlerFactory;
class Helper
{
public:

	static int asciiToDecimal(std::vector<byte> aclientSocketii);
	static int getMessageTypeCode(SOCKET clientSocket);
	static int getIntPartFromSocket(SOCKET clientSocket, int bytesNum);
	static void sendData(SOCKET clientSocket, std::vector<unsigned char> message);
	static void sendPostMessage(SOCKET clientSocket, const string& data);
	static std::vector<byte> decimalToAscii(int number);
	static std::vector<byte> getDataPartFromSocket(SOCKET clientSocket);
	static std::vector<byte> stringToByte(std::string data);
	static void sendError(SOCKET clientSocket, const string& data);


private:
	static char* getPartFromSocket(SOCKET clientSocket, int bytesNum);
	static char* getPartFromSocket(SOCKET clientSocket, int bytesNum, int flags);

};


#ifdef _DEBUG // vs add this define in debug mode
#include <stdio.h>
// Q: why do we need traces ?
// A: traces are a nice and easy way to detect bugs without even debugging
// or to understand what happened in case we miss the bug in the first time
#define TRACE(msg, ...) printf(msg "\n", __VA_ARGS__);
// for convenient reasons we did the traces in stdout
// at general we would do this in the error stream like that
// #define TRACE(msg, ...) fprintf(stderr, msg "\n", __VA_ARGS__);

#else // we want nothing to be printed in release version
#define TRACE(msg, ...) printf(msg "\n", __VA_ARGS__);
#define TRACE(msg, ...) // do nothing
#endif
