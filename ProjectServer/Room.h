#pragma once
#include "LoggedUser.h"
#include <string.h>
#include <iostream>
#include <vector>

typedef struct RoomData
{
    unsigned int id;
    std::string name;
    unsigned int maxPlayers;
    unsigned int numOfQuestionsInGame;
    unsigned int timePerQuestion;
    unsigned int isActive;
}RoomData;

class Room
{
public:
    Room();
    Room(const struct RoomData other);
    void setActive(unsigned int newActive);
    unsigned int getID() const;
    unsigned int getActive() const;
    std::vector<LoggedUser> getUsers() const;
    bool findUser(LoggedUser);
    void addUser(LoggedUser);
    void removeUser(LoggedUser);
    std::vector<std::string> getAllUsers();
    RoomData getRoomData() const;

private:
    struct RoomData m_metadata;
    std::vector<LoggedUser> m_users;
};
