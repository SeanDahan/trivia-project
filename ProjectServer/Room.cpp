#include "Room.h"

Room::Room()
{
}

Room::Room(const struct RoomData other)
{
    this->m_metadata.id = other.id;
    this->m_metadata.isActive = other.isActive;
    this->m_metadata.maxPlayers = other.maxPlayers;
    this->m_metadata.name = other.name;
    this->m_metadata.numOfQuestionsInGame = other.numOfQuestionsInGame;
    this->m_metadata.timePerQuestion = other.timePerQuestion;
}

void Room::setActive(unsigned int newActive)
{
    this->m_metadata.isActive = newActive;
}

unsigned int Room::getID() const
{
    return this->m_metadata.id;
}

unsigned int Room::getActive() const
{
    return this->m_metadata.isActive;
}

std::vector<LoggedUser> Room::getUsers() const
{
    return this->m_users;
}

bool Room::findUser(LoggedUser user)
{
    std::vector<LoggedUser>::iterator it = std::find(m_users.begin(), m_users.end(), user);
    return it != m_users.end();
}

void Room::addUser(LoggedUser newUser)
{
    this->m_users.push_back(newUser);
}

void Room::removeUser(LoggedUser user)
{
    if (findUser(user))
    {
        std::vector<LoggedUser>::iterator it = std::find(m_users.begin(), m_users.end(), user);
        this->m_users.erase(it);
    }
}

std::vector<std::string> Room::getAllUsers()
{
    std::vector<std::string> users;

    for (std::vector<LoggedUser>::iterator itr = this->m_users.begin(); itr != this->m_users.end(); itr++)
    {
        users.push_back(itr->getUsername());
    }
    return users;
}

RoomData Room::getRoomData() const
{
    return m_metadata;
}
