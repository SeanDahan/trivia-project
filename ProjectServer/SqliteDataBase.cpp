#include "SqliteDataBase.h"

SqliteDataBase::SqliteDataBase(const char* filePath) 
{

	if (sqlite3_open(filePath, &dbPointer) != SQLITE_OK) {
		dbPointer = nullptr;
		std::cout << "Failed to open DB\n";
	}
	// Iniate Relevant SQL Tables

	std::string query = "create table if not exists users (username text not null, password text not null, email text not null);";
	executeQuery(query.c_str(), nullptr, nullptr);
	query = "create table if not exists questions (id integer primarykey  auto increment, question text not null, firstAnswer text not null, secondAnswer text not null, thirdAnswer text not null, fourthAnswer text not null, correctAnswer integer not null);";
	executeQuery(query.c_str(), nullptr, nullptr);
	query = "create table if not exists statistics (id integer primarykey auto increment not null , username text not null, totalGames integer not null, totalAnswers integer not null, correctAnswers integer not null, averageAnswerTime float not null);";
	executeQuery(query.c_str(), nullptr, nullptr);
	query = "create table if not exists data (id integer primarykey auto increment not null , gameId integer primary key not null,username text not null, correctAnswers integer not null, totalAnswers integer not null, averageAnswerTime float not null);";
	executeQuery(query.c_str(), nullptr, nullptr);
	
	
	// Questions Part
	query = "insert into questions (question, firstAnswer, secondAnswer, thirdAnswer, fourthAnswer,  correctAnswer) VALUES (\"What is the ascii value of 'A'?\", \"65\", \"75\", \"100\", \"97\", 1)";
	executeQuery(query.c_str(), nullptr, nullptr);
	query = "insert into questions (question, firstAnswer, secondAnswer, thirdAnswer, fourthAnswer,  correctAnswer) VALUES (\"What is the third letter in the English alphabet?\", \"A\", \"S\", \"X\", \"C\", 4)";
	executeQuery(query.c_str(), nullptr, nullptr);
	query = "insert into questions (question, firstAnswer, secondAnswer, thirdAnswer, fourthAnswer,  correctAnswer) VALUES (\"What food is best?\", \"Pizza\", \"Ice Cream\", \"Shushi\", \"All of them are good\", 4)";
	executeQuery(query.c_str(), nullptr, nullptr);
	query = "insert into questions (question, firstAnswer, secondAnswer, thirdAnswer, fourthAnswer,  correctAnswer) VALUES (\"How old is Israel at 2021?\", \"75\", \"73\", \"65\", \"124\", 2)";
	executeQuery(query.c_str(), nullptr, nullptr);
	query = "insert into questions (question, firstAnswer, secondAnswer, thirdAnswer, fourthAnswer,  correctAnswer) VALUES (\"What is the basic output command in C?\", \"printf\", \"print\", \"console.log\", \"Console.WriteLine\", 1)";
	executeQuery(query.c_str(), nullptr, nullptr);
}

SqliteDataBase::SqliteDataBase()
{
}

SqliteDataBase::~SqliteDataBase() {
	sqlite3_close(dbPointer);
	dbPointer = nullptr;
}


bool SqliteDataBase::executeQuery(const char* query, callbackFunction callBack, void* dataPointer) {
	char* errorMessage = nullptr;
	int resCode = 0;
	if ((resCode = sqlite3_exec(dbPointer, query, callBack, dataPointer, &errorMessage) != SQLITE_OK)) {
		std::cout << "Error While Parsing Query " << resCode << " with " << errorMessage;
		return false;
	}
	return true;
}



bool SqliteDataBase::doesUserExist(std::string name) {
	std::string query = "select count(*) from users where username=\"" + name + "\";";
	bool dataPointer = false;
	executeQuery(query.c_str(), SqliteDataBase::singleBoolCallBack, &dataPointer);
	return dataPointer;
}

bool SqliteDataBase::doesPassMatch(std::string name, std::string pass) {
	std::string query = "select count(*) from users where username=\"" + name + "\" and password=\"" + pass + "\";";
	bool dataPointer = false;
	executeQuery(query.c_str(), SqliteDataBase::singleBoolCallBack, &dataPointer);
	return dataPointer;
}

bool SqliteDataBase::addNewUser(std::string name, std::string pass, std::string email) {
	std::string query = "insert into users (username,password,email) VALUES (\"" + name + "\",\"" + pass + "\",\"" + email + "\");";
	return executeQuery(query.c_str(), nullptr, nullptr);
}

std::list<Question> SqliteDataBase::getQuestions(int amountOfQuestions) {
	std::string query = "select * from questions limit " + std::to_string(amountOfQuestions) + ";";
	std::list<Question> questionsList;
	executeQuery(query.c_str(), SqliteDataBase::questionsCallback, &questionsList);
	return questionsList;
}

float SqliteDataBase::getPlayerAverageAnswerTime(std::string username) {
	std::string query = "select averageAnswerTime from statistics where username=\"" + username + "\";";
	float dataPointer = 0.0f;
	executeQuery(query.c_str(), SqliteDataBase::singleFloatCallBack, &dataPointer);
	return dataPointer;
}

int SqliteDataBase::getNumOfCorrectAnswer(std::string username) {
	std::string query = "select correctAnswers from statistics where username=\"" + username + "\";";
	int dataPointer = 0;
	executeQuery(query.c_str(), SqliteDataBase::singleFloatCallBack, &dataPointer);
	return dataPointer;
}

int SqliteDataBase::getNumOfTotalAnswers(std::string username) {
	std::string query = "select totalAnswers from statistics where username=\"" + username + "\";";
	int dataPointer = 0;
	executeQuery(query.c_str(), SqliteDataBase::singleFloatCallBack, &dataPointer);
	return dataPointer;
}

std::string SqliteDataBase::getBestPlayerUsername() {
	std::string query = "select username from statistics where correctAnswers = ( select max(correctAnswers) from statistics);";
	std::string username = "";
	executeQuery(query.c_str(), SqliteDataBase::singleStringCallback, &username);
	return username;
}

int SqliteDataBase::getNumOfPlayerGames(std::string username) {
	std::string query = "select totalGames from statistics where username=\"" + username + "\";";
	int dataPointer = 0;
	executeQuery(query.c_str(), SqliteDataBase::singleFloatCallBack, &dataPointer);
	return dataPointer;
}

void SqliteDataBase::insertScore(std::string username, int gameId, int amountOfAnswers, int amountOfCorrectAnswers, float averageAnswerTime) {
	std::string query = "insert into data (gameid, username, correctAnswers, totalAnswers, averageAnswerTime) VALUES(" + std::to_string(gameId) + ", " + username + ", " + std::to_string(amountOfCorrectAnswers) + ", " + std::to_string(amountOfAnswers) + ", " + std::to_string(averageAnswerTime) + ");";
	// query = "create table if not exists data (id integer primary key not null autoincrement, gameId integer primary key not null,username text not null, correctAnswers integer not null, totalAnswers integer not null, averageAnswerTime float not null);";
	executeQuery(query.c_str(), NULL, NULL);

	int amount = 0, amountCorrect = 0, averageTime = 0;
	query = "select totalAnswers from statistics;";
	executeQuery(query.c_str(), singleIntCallBack, &amount);
	query = "select totalCorrectAnswers from statistics;";
	executeQuery(query.c_str(), singleIntCallBack, &amountCorrect);
	query = "select averageAnswerTime from statistics;";
	executeQuery(query.c_str(), singleFloatCallBack, &averageTime);
	amount += amountOfAnswers;
	amountCorrect += amountOfCorrectAnswers;
	averageTime += averageAnswerTime;
	averageTime /= 2;
	
	query = "update statistics set totalAnswers=" + std::to_string(amount) + ", correctAnswers=" + std::to_string(amountCorrect) + ", averageAnswerTime=" + std::to_string(averageTime) + "where username=\"" + username + "\";";
	executeQuery(query.c_str(), NULL, NULL);
}

int SqliteDataBase::singleBoolCallBack(void* data, int argc, char** argv, char** azColName) {
	if (argc != 0 && !strcmp(argv[0], "1")) {
		bool* existRes = static_cast<bool*>(data);
		*existRes = bool(atoi(argv[0]));
	}
	return 0;
}

int SqliteDataBase::singleIntCallBack(void* data, int argc, char** argv, char** azColName) {
	if (argc != 0 && !strcmp(argv[0], "1")) {
		int* existRes = static_cast<int*>(data);
		*existRes = atoi(argv[0]);
	}
	return 0;
}

int SqliteDataBase::singleFloatCallBack(void* data, int argc, char** argv, char** azColName) {
	if (argc != 0 && !strcmp(argv[0], "1")) {
		float* existRes = static_cast<float*>(data);
		*existRes = atof(argv[0]);
	}
	return 0;
}

int SqliteDataBase::questionsCallback(void* data, int argc, char** argv, char** azColName) {
	if (argc != 0) {
		std::list<Question>* questionsList = static_cast<std::list<Question>*>(data);
		for (size_t i = 0; i < argc; i++) {
			questionsList->push_back(Question(argv[1], argv[2], argv[3], argv[4], argv[5], atoi(argv[6])));
		}
	}
	return 0;
}

int SqliteDataBase::singleStringCallback(void* data, int argc, char** argv, char** azColName) {
	if (argc != 0) {
		std::string* username = static_cast<std::string*>(data);
		*username = argv[0];
	}
	return 0;
}