#pragma once
#include <unordered_map>
#include <iostream>
using namespace std;

class convertBinaryToHex
{
public:
	static string convertBinToHex(string bin);
	static void createMap(unordered_map<string, char>* um);
};

