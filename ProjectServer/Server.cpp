#include "Server.h"

Server::Server()
{
	m_database = new SqliteDataBase("MyDB.sqlite");
	m_communicator = new Communicator(m_database);
}

Server::~Server()
{
}

void Server::run() {
	m_communicator->startHandleRequests();
}
