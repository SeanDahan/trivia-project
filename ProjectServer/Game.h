#pragma once
#include <string.h>
#include <iostream>
#include <vector>
#include <map>
#include "Question.h"
#include "LoggedUser.h"

typedef struct GameData
{
    Question currentQuestion;
    unsigned int correctAnswerCount;
    unsigned int wrongAnswerCount;
    unsigned int averageAnswerTime;
    GameData() : currentQuestion(Question("","","","","",0)), correctAnswerCount(0), wrongAnswerCount(0), averageAnswerTime(0)
    {}
    GameData(Question question) : currentQuestion(Question(question)), correctAnswerCount(0), wrongAnswerCount(0), averageAnswerTime(0)
    {}
    void operator=(const GameData& other) {
        this->currentQuestion = other.currentQuestion;
        this->correctAnswerCount = other.correctAnswerCount;
        this->wrongAnswerCount = other.wrongAnswerCount;
        this->averageAnswerTime = other.averageAnswerTime;
    }
    bool operator==(const GameData& other) {
        return this->currentQuestion == other.currentQuestion &&
            this->correctAnswerCount == other.correctAnswerCount &&
            this->wrongAnswerCount == other.wrongAnswerCount &&
            this->averageAnswerTime == other.averageAnswerTime;
    }
}GameData;


class Game
{
public:
    Game();
    
    void addPlayer(LoggedUser user, GameData data);
    Question getQuestionForUser(LoggedUser user);
    void submitAnswer(LoggedUser user, std::string answer);
    void removePlayer(LoggedUser user);
    bool userExist(LoggedUser user);
    bool operator==(const Game& rhs) const;
    //bool operator<(const Game& rhs) const;
    std::map<LoggedUser, GameData> getPlayers() const { return m_players; };

private:
    std::vector<Question> m_questions;
    std::map<LoggedUser, GameData> m_players;
};

