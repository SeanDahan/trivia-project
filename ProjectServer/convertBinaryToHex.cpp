#include "convertBinaryToHex.h"


void solve(int num)
{
    long int hexa_val = 0, i = 1, r;
    while (num != 0)
    {
        r = num % 10;
        hexa_val = hexa_val + r * i;
        i = i * 2;
        num = num / 10;
    }
    cout << "Equivalent hexadecimal value: " << hexa_val;

}
