#include "RoomAdminRequestHandler.h"

RoomAdminRequestHandler::RoomAdminRequestHandler(RequestHandlerFactory factory)
    : m_handlerFactory(factory), m_roomManager(factory.getRoomManager())
{
}

RoomAdminRequestHandler::~RoomAdminRequestHandler()
{
}

bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo request)
{
    if (request.id == GET_ROOM_STATE_T || request.id == START_GAME_T || request.id == CLOSE_ROOM_T)
    {
        return true;
    }
    return false;
}

RequestResult RoomAdminRequestHandler::handleRequest(RequestInfo request)
{
    RequestResult result;
    if (isRequestRelevant(request))
    {
        switch (request.id)
        {
        case CLOSE_ROOM_T:
        {
            result = closeRoom(request);
            break;
        }
        case START_GAME_T:
        {
            result = startGame(request);
            break;
        }
        case GET_ROOM_STATE_T:
        {
            result = getRoomState(request);
            break;
        }
        }
        result.newHandler = this;
    }
    else
    {
        ErrorResponse errorResponse;
        errorResponse.message = "The request is irrelevant";
        result.buffer = JsonResponsePacketSerializer::serializeResponse(errorResponse);
        result.newHandler = nullptr;
    }
    return result;
}

RequestResult RoomAdminRequestHandler::closeRoom(RequestInfo request)
{
    RequestResult result;
    CloseRoomResponse closeRoomResponse;
    closeRoomResponse.status = CLOSE_ROOM_T;
    std::vector<std::string> players = this->m_room.getAllUsers();
    for (std::string player : players)
    {
        m_room.removeUser(this->m_handlerFactory.getLoginManager().getUser(player));
    }
    this->m_roomManager.deleteRoom(m_room.getID());
    result.buffer = JsonResponsePacketSerializer::serializeResponse(closeRoomResponse);
    return result;
}

RequestResult RoomAdminRequestHandler::startGame(RequestInfo request)
{
    RequestResult result;
    StartGameResponse startGameResponse;
    startGameResponse.status = START_GAME_T;
    m_room.setActive(1);
    result.buffer = JsonResponsePacketSerializer::serializeResponse(startGameResponse);
    return result;
}

RequestResult RoomAdminRequestHandler::getRoomState(RequestInfo request)
{
    RequestResult result;
    GetRoomStateResponse getRoomStateResponse;
    getRoomStateResponse.hasGameBegun = false;
    getRoomStateResponse.players = this->m_room.getAllUsers();
    getRoomStateResponse.questionCount = this->m_room.getRoomData().numOfQuestionsInGame;
    getRoomStateResponse.answerTimeOut = this->m_room.getRoomData().timePerQuestion;
    getRoomStateResponse.status = GET_ROOM_STATE_T;
    result.buffer = JsonResponsePacketSerializer::serializeResponse(getRoomStateResponse);
    return result;
}
