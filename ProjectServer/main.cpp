#pragma comment (lib, "ws2_32.lib")
#include "WSAInitializer.h"
#include <iostream>
#include <thread>
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "ResponseStructs.h"
#include "SqliteDataBase.h"
#include "Server.h"
#include "Helper.h"

int main()
{
	try
	{
		WSAInitializer wsa_init;
		Server server;
		server.run();
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was thrown in function: " << e.what() << std::endl;
	}
	catch (...)
	{
		std::cout << "Unknown exception in main !" << std::endl;
	}
	system("pause");
	return 0;
}
