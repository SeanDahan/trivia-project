#pragma once
#include "IRequestHandler.h"
#include "ResponseStructs.h"
#include "RequestStructs.h"
#include "JsonResponsePacketSerializer.h"
#include "RequestHandlerFactory.h"
#include "LoginManager.h"

class RequestHandlerFactory;
class LoginRequestHandler :
    public IRequestHandler
{
public:
    LoginRequestHandler(RequestHandlerFactory&);
    ~LoginRequestHandler();

    virtual bool isRequestRelevant(RequestInfo);
    virtual RequestResult handleRequest(RequestInfo);
    RequestResult login(RequestInfo);
    RequestResult signup(RequestInfo);
private:
    LoginManager& m_loginManager;
    RequestHandlerFactory& m_handlerFactory;
};

