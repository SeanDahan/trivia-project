﻿﻿using System;  
using System.Net;  
using System.Net.Sockets;  
using System.Threading;  
using System.Text;  
  
public class AsynchronousClient
{  
    // The port number for the remote device.  
    private const int port = 3611;
    private const string ip = "127.0.0.1";

    public static Socket StartClient() 
    {  
        try 
        {  
            IPAddress ipAddress = IPAddress.Parse(ip);  
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);  

            Socket client = new Socket(ipAddress.AddressFamily,  
                SocketType.Stream, ProtocolType.Tcp);

            client.Connect(remoteEP);
            return client;
        } 
        catch (Exception e) 
        {  
            Console.WriteLine(e.ToString());  
        }
        return null;
    }  
 
    public static string GetMessage(Socket client)
    {
        byte[] bytes = new byte[1024];
        int bytesRec = client.Receive(bytes);
        return Encoding.ASCII.GetString(bytes, 0, bytesRec);
    }

    public static void SendMessage(Socket client, byte[] byteData)
    {
        client.Send(byteData);
    }
}
