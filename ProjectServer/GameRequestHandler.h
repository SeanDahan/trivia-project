#pragma once
#include "RequestStructs.h"
#include "RequestHandlerFactory.h"
#include "IRequestHandler.h"
#include "GameManager.h"
#include "LoggedUser.h"

class RequestHandlerFactory;
class IRequestHandler;

class GameRequestHandler: public IRequestHandler
{
public:
	GameRequestHandler(RequestHandlerFactory&);
	virtual ~GameRequestHandler();

	virtual bool isRequestRelevant(RequestInfo);
	virtual RequestResult handleRequest(RequestInfo);
private:
	Game m_game;
	LoggedUser m_user;
	GameManager& m_gameManager;
	RequestHandlerFactory& m_requestHandleFactory;
	RequestResult getQuestion(RequestInfo);
	RequestResult submitAnswer(RequestInfo);
	RequestResult getGameResults(RequestInfo);
	RequestResult leaveGame(RequestInfo);
};