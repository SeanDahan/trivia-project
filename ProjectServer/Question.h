#pragma once
#include <string>
#include <vector>

class Question 
{
public:
	Question(std::string, std::string, std::string, std::string, std::string, int);
	virtual ~Question();

	std::string getQuestion() const { return m_question; };
	std::string getCorrectAnswer() const { return m_possibleAnswers[m_correctAnswer-1]; };
	std::vector<std::string> getPossibleAnswers() const { return m_possibleAnswers; };
	bool operator==(const Question other) const {
		return this->m_question == other.m_question &&
			this->m_possibleAnswers.size() == other.m_possibleAnswers.size() &&
			std::equal(this->m_possibleAnswers.begin(), this->m_possibleAnswers.end(), other.m_possibleAnswers.begin()) &&
			this->m_correctAnswer == other.m_correctAnswer;

	}
private:
	unsigned int m_correctAnswer;
	std::string m_question;
	std::vector<std::string> m_possibleAnswers;
};

