#include "MenuRequestHandler.h"

MenuRequestHandler::MenuRequestHandler(RequestHandlerFactory& factory) : m_requestHandleFactory(factory), m_roomManager(factory.getRoomManager()), m_statisticsManager(factory.getStatisticsManager())
{
    this->m_user = factory.getLoginManager().getLoggedUser().rbegin()->getUsername();
}

MenuRequestHandler::~MenuRequestHandler()
{
}

bool MenuRequestHandler::isRequestRelevant(RequestInfo request)
{
    switch (request.id) {
    case CREATE_ROOM_T:
    case GET_ROOMS_T:
    case GET_PLAYERS_IN_ROOM_T:
    case JOIN_ROOM_T:
    case GET_PERSONAL_STATS_T:
    case LOGOUT_T:
    case HIGH_SCORE_T:
        return true;
    default:
        return false;
    }
    return false;
}

RequestResult MenuRequestHandler::handleRequest(RequestInfo request)
{
    
    switch (request.id) 
    {
    case CREATE_ROOM_T: return createRoom(request);
    case GET_ROOMS_T: return getRooms(request);
    case GET_PLAYERS_IN_ROOM_T: return getPlayersInRoom(request);
    case JOIN_ROOM_T: return joinRoom(request);
    case GET_PERSONAL_STATS_T: return getPersonalStats(request);
    case LOGOUT_T: return signout(request);
    case HIGH_SCORE_T: return getHighScore(request);
    default: return RequestResult();
    }
    
}

RequestResult MenuRequestHandler::signout(RequestInfo info)
{
    RequestResult result;
    LogoutResponse logoutResponse;
    logoutResponse.status = LOGOUT_T;
    this->m_requestHandleFactory.getLoginManager().logout(m_user.getUsername());
    result.buffer = JsonResponsePacketSerializer::serializeResponse(logoutResponse);
    result.newHandler = new LoginRequestHandler(this->m_requestHandleFactory);
   
    return result;
}

RequestResult MenuRequestHandler::getRooms(RequestInfo info)
{
    RequestResult result;
    GetRoomResponse getRoomResponse;
    getRoomResponse.status = GET_ROOMS_T;
    getRoomResponse.rooms = this->m_roomManager.getRooms();
    result.buffer = JsonResponsePacketSerializer::serializeResponse(getRoomResponse);
    result.newHandler = this;
    return result;
}

RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo info)
{
    GetPlayersInRoomRequest request = JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(info.buffer);
    RequestResult result;
    GetPlayersInRoomResponse playersInRoomResponse;
    playersInRoomResponse.status = GET_PLAYERS_IN_ROOM_T;
    playersInRoomResponse.players = this->m_roomManager.getPlayersInRoom(request.roomID);
    result.buffer = JsonResponsePacketSerializer::serializeResponse(playersInRoomResponse);
    result.newHandler = this;
    return result;
}

RequestResult MenuRequestHandler::getPersonalStats(RequestInfo info)
{
    RequestResult result;
    GetPersonalStatsResponse getStatsResponse;
    getStatsResponse.status = GET_PERSONAL_STATS_T;
    getStatsResponse.statistics = this->m_statisticsManager.getUserStatistics(m_user.getUsername());
    result.buffer = JsonResponsePacketSerializer::serializeResponse(getStatsResponse);
    result.newHandler = this;
    return result;
}

RequestResult MenuRequestHandler::getHighScore(RequestInfo info)
{
    RequestResult result;
    HighScoreResponse highScoreResponse;
    highScoreResponse.status = GET_PLAYERS_IN_ROOM_T;
    highScoreResponse.statistics = this->m_statisticsManager.getHighScore();
    result.buffer = JsonResponsePacketSerializer::serializeResponse(highScoreResponse);
    result.newHandler = this;
    return result;
}

RequestResult MenuRequestHandler::joinRoom(RequestInfo info)
{
    RequestResult result;
    JoinRoomRequest request = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(info.buffer);
    if (this->m_roomManager.getPlayersInRoom(request.roomID).size() < m_roomManager.getRoom(request.roomID).getRoomData().maxPlayers)
    {
        this->m_roomManager.getRoom(request.roomID).addUser(m_user);
        JoinRoomResponse joinRoomResponse;
        joinRoomResponse.status = JOIN_ROOM_T;
        result.buffer = JsonResponsePacketSerializer::serializeResponse(joinRoomResponse);
        result.newHandler = this;
    }
    else
    {
        ErrorResponse errorResponse;
        errorResponse.message = "Can't join Room" + request.roomID;
        result.buffer = JsonResponsePacketSerializer::serializeResponse(errorResponse);
        result.newHandler = this;
    }
    return result;
}

RequestResult MenuRequestHandler::createRoom(RequestInfo info)
{
    RequestResult result;
    struct RoomData newRoom;
    CreateRoomRequest request = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(info.buffer);
    newRoom.name = request.roomName;
    newRoom.maxPlayers = request.maxUsers;
    newRoom.numOfQuestionsInGame = request.questionCount;
    newRoom.timePerQuestion = request.answerTimeOut;
    newRoom.isActive = false;
    this->m_roomManager.createRoom(m_user, newRoom);
    CreateRoomResponse createRoomResponse;
    createRoomResponse.status = CREATE_ROOM_T;
    result.buffer = JsonResponsePacketSerializer::serializeResponse(createRoomResponse);
    result.newHandler = this;
    return result;
}
