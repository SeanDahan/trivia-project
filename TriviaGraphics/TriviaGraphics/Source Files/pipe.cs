﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Pipes;
using System.IO;
using System.Threading;

namespace TriviaGraphics
{
    class pipe
    {
        NamedPipeServerStream pipeServer;
        StreamString ss;

        public pipe()
        {
            pipeServer = new NamedPipeServerStream("triviaPipe", PipeDirection.InOut, 1);
            ss = new StreamString(pipeServer);
        }

        public bool connect()
        {
            // Wait for a client to connect
            pipeServer.WaitForConnection();

            return pipeServer.IsConnected;
        }

        public bool isConnected()
        {
            return pipeServer.IsConnected;
        }

        public string getEngineMessage()
        {
            if (!isConnected())
            {
                return "";
            }

            string res = ss.getStringFromEngine();

            Console.WriteLine(res);

            return res;
        }

        public void sendEngineRequest(byte[] request)
        {
            if (!isConnected())
            {
                return;
            }

            ss.sendRequestToEngine(request);
        }

        public void close()
        {
            pipeServer.Close();
        }
    }



    public class StreamString
    {
        private Stream ioStream;
        private Encoding streamEncoding;

        public StreamString(Stream ioStream)
        {
            this.ioStream = ioStream;
            streamEncoding = new ASCIIEncoding();
        }

        public string getStringFromEngine()
        {
            byte[] inBuffer = new byte[1024];
            ioStream.Read(inBuffer, 0, 1024);


            String MyString = Encoding.ASCII.GetString(inBuffer).TrimEnd((Char)0);
            return Encoding.ASCII.GetString(inBuffer).TrimEnd((Char)0);
        }

        public void sendRequestToEngine(byte[] inBuffer)
        {
            try
            {
                ioStream.Write(inBuffer, 0, inBuffer.Length);

                ioStream.Flush();
            }
            catch
            {

            }
        }
    }
}
